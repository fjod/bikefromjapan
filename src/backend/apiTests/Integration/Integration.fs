module apiTests.Integration

open System.Net.Http
open System.Text
open FsTypes
open Microsoft.AspNetCore.TestHost
open Microsoft.Extensions.Configuration
open TsInterfaces
open Xunit
open Newtonsoft.Json
open backend.App
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.DependencyInjection
open System
open System.IO
open Fixtures

let createHost() =
    WebHostBuilder()
        .UseContentRoot(Directory.GetCurrentDirectory()) 
        .UseEnvironment("Test")
        .ConfigureAppConfiguration(configureAppConfiguration)
        .Configure(Action<IApplicationBuilder> configureApp)
        .ConfigureServices(Action<IServiceCollection> configureServices)
        .UseUrls("http://localhost:1234")        

let getClient() =
    task{
    let host = createHost()
    let server = new TestServer(host)
    do! server.Host.StartAsync()
    return server.CreateClient()
    }
    
let clientGetRequest (client: HttpClient) (route:string) =
    task{
      let! ping = client.GetAsync route
      ping.EnsureSuccessStatusCode() |> ignore
      let! text = ping.Content.ReadAsStringAsync()
      return text
    }
    
let clientPostRequest (client: HttpClient) (route:string) (content:StringContent) =
    task{
      let! ping = client.PostAsync(route, content)
      ping.EnsureSuccessStatusCode() |> ignore
      let! text = ping.Content.ReadAsStringAsync()
      return text
    }
    
[<Fact>]
let ``GET /ping should respond pong`` () =
   task{
    let! client = getClient()
    
    let! ping = clientGetRequest client "ping"  
    ping |> shouldEqual "pong"    
    }

   
   
[<Fact>]
let ``POST /registration returns working token`` () =    
    task{
    let host = createHost()    
    use server = new TestServer(host)
    let config = server.Services.GetService<IConfiguration>()
    let connString = config.GetValue "MySqlConnectionString"
    use client = server.CreateClient()
    
    backend.Database.DB_deleteUser ("sampleMail@mail.com", connString) |> Async.AwaitTask |> ignore

    let regData = TS_RegistrationInput("sampleMail@mail.com", "password")
    use content = new StringContent(JsonConvert.SerializeObject(regData), Encoding.UTF8, "application/json");
    let! ret = clientPostRequest client "register" content    
    let token = JsonConvert.DeserializeObject<TS_RegistrationOutput>(ret)
    shouldEqual token.Message "register ok"
    Assert.NotNull token.Token
    backend.Database.DB_deleteUser ("sampleMail@mail.com", connString) |> Async.AwaitTask |> ignore
    
    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.Token)
    let! authPong = clientGetRequest client "authorizedPing"
    Assert.True(authPong.Contains "sampleMail@mail.com")    
    }

let conv input =
        JsonConvert.DeserializeObject<TS_BikeOutput[]>(input) |> Seq.toList
        
[<Fact>]
let ``Get auct data with different inputs`` () =
    task{
    let! client = getClient()
    
    let! empty = clientGetRequest client "auctData"
    let emptyData = empty |> conv    
    Assert.True(emptyData.Length = 0)
    
    let! ret = clientGetRequest client "auctData?man=H"
    let retData = ret |> conv        
    let notHonda = retData |> Seq.map (fun b -> ManufacturerHelper.ManufacturerFromString (Some b.Manufacturer)) |> Seq.filter (fun m -> m <> Some Honda) |> Seq.length |> (=) 0    
    Assert.True(notHonda)
    
    let! ret = clientGetRequest client "auctData?man=H&startYear=2010"
    let retData = ret |> conv        
    let hondasOlder2010 =  retData |> Seq.filter (fun b -> b.Year < 2010 && b.Year <> 0) |> Seq.length |> (=) 0
    Assert.True(hondasOlder2010)
    
    let! ret = clientGetRequest client "auctData?man=H&stopYear=2010"
    let retData = ret |> conv        
    let hondasYounger2010 =  retData |> Seq.filter (fun b -> b.Year > 2010 && b.Year <> 0) |> Seq.length |> (=) 0
    Assert.True(hondasYounger2010)
    
    let! ret = clientGetRequest client "auctData?man=H&stopYear=2015&startYear=2010"
    let retData = ret |> conv        
    let hondas2010_2015 =  retData |> Seq.filter (fun b -> b.Year < 2010 && b.Year <> 0 && b.Year > 2015) |> Seq.length |> (=) 0
    Assert.True(hondas2010_2015)
    }
    
[<Theory>]
[<InlineData(3)>]
[<InlineData(1)>]
let ``Get auct data for models`` (modelCount : int) =
    task {
    let testResult index (list:TS_BikeModel list) (output:TS_BikeOutput list) =
        let currentModel = list |> List.find (fun b -> b.Id = index)
        let auctDataForThisModel = output |> List.filter (fun f -> f.Name = currentModel.Name)
        Console.WriteLine $"For {currentModel.Id} {currentModel.Name} there is {auctDataForThisModel.Length} auct records"
        Assert.True(auctDataForThisModel.Length > 0)
       

    let! client = getClient()
    let! bikeModels = clientGetRequest client "models"
    let bikeModels =  bikeModels |> JsonConvert.DeserializeObject<TS_BikeModels>
    let hondas = Seq.toList bikeModels.BikeModels |> Seq.filter (fun b -> ManufacturerHelper.ManufacturerFromString (Some b.Manufacturer) = Some Honda) |> Seq.toList    
    let rand =  Random()
    let randModelsIds = [| for _ in 1 .. modelCount -> hondas[rand.Next(0, hondas.Length)].Id |]
    use content = new StringContent(JsonConvert.SerializeObject(randModelsIds), Encoding.UTF8, "application/json")        
    
    let! ret = clientPostRequest client "auctData" content    
    let ret = ret |> conv
    randModelsIds |> Array.iter (fun i -> testResult i hondas ret)
    }
    