module HttpFunc
open System.Net
open System.Net.Http
open Xunit

    

let runTask task = 
    task
    |> Async.AwaitTask
    |> Async.RunSynchronously

let createRequest (method : HttpMethod) (path : string) = 
    let url = "http://127.0.0.1" + path
    new HttpRequestMessage(method, url)

let ensureSuccess (response : HttpResponseMessage) = 
    if not response.IsSuccessStatusCode then 
        let resp = response.Content.ReadAsStringAsync()
        resp.Result 
        |> failwithf "%A"
    else response

let isStatus (code : HttpStatusCode) (response : HttpResponseMessage) = 
    Assert.Equal(code, response.StatusCode)
    response

let isOfType (response : HttpResponseMessage) (contentType : string)  = 
    Assert.Equal(contentType, response.Content.Headers.ContentType.MediaType)
    response

let readText (response : HttpResponseMessage) =
    let msg = response.Content.ReadAsStringAsync()
    msg.Result 