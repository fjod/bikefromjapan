
module Tests

open System
open System.IO
open System.Text.Json
open DataAccess
open DataAccess.Repositories
open FsDomain.Interfaces
open FsTypes
open Microsoft.EntityFrameworkCore
open Xunit
open backend.BikeDownloader
open backend.RecurrentJob
open backend.Database
open backend.Security.Jwt

let options =
    let one = DbContextOptionsBuilder<BikesContext>()
    one.UseInMemoryDatabase(Guid.NewGuid().ToString()).Options
    
let context = new BikesContext(options)
do context.Database.EnsureCreated() |> ignore
let bikesRepo = BikesRepository(context) :> IBikeRepository
let userRepo = UserRepository(context) :> IUserRepository

type BikeForDeserialization = {
    manufacturer : string
    model : string
    year : int
    key:string
    mileage : int
    image : string
}

let bikeFromDes (b:BikeForDeserialization) : Bike option =
                    let man =  ManufacturerHelper.ManufacturerFromLetter b.manufacturer
                    match man with
                    | Some m -> 
                        Some {
                            Image = b.image
                            Mileage = b.mileage
                            Manufacturer = m
                            Model = b.model
                            Year = b.year
                            Key = b.key
                        }
                     | None -> None

let path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
let path2 = Path.Combine(path, "japBikesFromAzure.json")
let bikes = JsonSerializer.Deserialize<System.Collections.Generic.List<BikeForDeserialization>>(File.OpenRead(path2))
            |> Seq.toList
            |> List.map bikeFromDes
            |> List.choose id


type TestDownloader() =
    interface IDownloadBikes with
        member this.Get(startYear : string , stopYear : string , manLetter : string, _ : LogFunc) =
            async{
                let man = ManufacturerHelper.ManufacturerFromLetter manLetter
                match man with
                | None -> return []                
                | Some m ->
                    let ret = bikes |> List.where (fun b -> b.Manufacturer = m && b.Year >= int startYear && b.Year <= int stopYear) 
                    return ret
            }
            
            
            
let downloader = TestDownloader() :> IDownloadBikes
        
// [<Fact>]
let ``Download func in API`` () =
    async{
        do! Download bikesRepo downloader 10000 FakeLog
        let! count = context.Bikes.CountAsync() |> Async.AwaitTask
        Assert.Equal(count,65)
        return ()
    }
     
// [<Theory>]
// [<InlineData("2010_2020_H.json", 97)>]
// [<InlineData("2015_2020_Y.json", 46)>]
// [<InlineData("2010_2015_S.json", 50)>]
let ``Deserialization func`` (fileName : string, result : int) =
    let path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
    let path2 = Path.Combine(path, "jsonExamples", fileName)
    let contentString = File.ReadAllText(path2)
    let ret = deserialize(contentString, String.Empty)
    Assert.NotEmpty(ret)
    Assert.Equal(ret.Length, result)
    ()
    
// [<Fact>]
let ``Create reg data`` () =
    let tokenResult = generateToken "test@email.com"
    let ret = createRegDataOutput (Ok "") tokenResult.Token
    Assert.True(ret.Token = tokenResult.Token)