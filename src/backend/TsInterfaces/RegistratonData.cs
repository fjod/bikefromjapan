﻿using Reinforced.Typings.Attributes;
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedType.Global

namespace TsInterfaces;

[TsInterface] 
 public class TS_RegistrationInput
 {
     public string Email { get; }
     public string Password { get; }

     public TS_RegistrationInput(string email, string password)
     {
         Email = email;
         Password = password;
     }
 }
 
[TsInterface] 
public class TS_RegistrationOutput
{
    public string Message { get; }
    public string Token { get; }

    public TS_RegistrationOutput(string message, string token)
    {
        Message = message;
        Token = token;
    }
}