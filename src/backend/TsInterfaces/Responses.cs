using Reinforced.Typings.Attributes;

namespace TsInterfaces;

[TsInterface]
public class TS_BikeModelsResult {
    public List<TS_BikeOutput> Response { get; set; }  
}

[TsInterface]
public class TS_CommonBackendResult {
    public string Message { get; set; }
}