using Reinforced.Typings.Attributes;

namespace TsInterfaces;

[TsInterface] 
public class TS_UserBikeInput
{
    public List<int> BikeIds { get; }

    public TS_UserBikeInput(List<int> bikeIds)
    {
        BikeIds = bikeIds;
    }
}

[TsInterface]
public class TS_BikeInput
{
    public string Name { get; }
    public int StartYear { get; }
    public int EndYear { get; }
    public string Manufacturer { get; }
    
    public int Id { get; }

    public TS_BikeInput(string name, int startYear, int endYear, string manufacturer, int id)
    {
        Name = name;
        StartYear = startYear;
        EndYear = endYear;
        Manufacturer = manufacturer;
        Id = id;
    }
}

[TsInterface]
public class TS_BikeModel
{
    public int Id { get; }
    public string Manufacturer { get; }
    public string Name { get; }
    public int StartYear { get; }
    public int StopYear { get; }

    public TS_BikeModel(string manufacturer, string name, int startYear, int stopYear, int id)
    {
        Manufacturer = manufacturer;
        Name = name;
        StartYear = startYear;
        StopYear = stopYear;
        Id = id;
    }
}

[TsInterface]
public class TS_BikeModels
{
    public List<string> Manufacturers { get; }

    public int MinYear { get; }

    public int MaxYear { get; }

    public List<TS_BikeModel> BikeModels { get; }

    public TS_BikeModels(List<string> manufacturers, int minYear, int maxYear, List<TS_BikeModel> bikeModels)
    {
        Manufacturers = manufacturers;
        MinYear = minYear;
        MaxYear = maxYear;
        BikeModels = bikeModels;
    }
}

[TsInterface]
public class TS_BikeOutput
{
    public string Manufacturer { get; }
    public string Key { get; }
    public string Name { get; }
    public string Image { get; }
    public int Year { get; }
    public int Mileage { get; }

    public TS_BikeOutput(string manufacturer, string key, string name, string image, int year, int mileage)
    {
        Manufacturer = manufacturer;
        Key = key;
        Name = name;
        Image = image;
        Year = year;
        Mileage = mileage;
    }
}

