namespace Domain.Objects;

public record MetaData(string ranking_status);  