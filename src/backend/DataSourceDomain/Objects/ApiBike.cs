using System.Collections.Generic;

namespace Domain.Objects;

public record BikesRoot (List<APIBike> Bikes);

public record APIBike(MetaData metaData, Id _id, string auction, int bid_number, object category,
    string condition_engine,
    string condition_exterior, string condition_frame, string condition_front, string condition_parts,
    string condition_rear,
    string date, int? engine_volume, int? final_bid, int? final_bid_rub, string frame_number, string key, object lane,
    string manufacturer, int? mileage, string model, string model_alise, string preview_image, string rank,
    string result,
    int? starting_bid, int? starting_bid_rub, object state, int? year);