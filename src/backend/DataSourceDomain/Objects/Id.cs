using System.Text.Json.Serialization;

namespace Domain.Objects;

public class Id
{
    [JsonPropertyName("$oid")]
    public string Oid { get; set; }
}