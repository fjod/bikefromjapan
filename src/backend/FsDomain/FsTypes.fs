﻿module FsTypes

type Manufacturer =
    | Honda
    | Suzuki
    | Kawasaki
    | Yamaha
    | Unknown
    
  

type Bike = {
    Manufacturer : Manufacturer
    Model : string
    Year : int
    Key:string
    Mileage : int
    Image : string
}

type BikeModel = {
    Manufacturer : Manufacturer
    Model : string
    StartYear : int
    StopYear : int
    Id : int
}

type User = {
    Email :string
    Password : string
    Salt : string    
}

type UserAuctData = {
    Email : string
    Bikes : System.Collections.Generic.List<Bike>
}

type DbResult<'t> = {
    Payload : 't
    Error : string
}

module ManufacturerHelper =
    let ManufacturerFromString (word:string option) : Manufacturer option =
        match word with
        | Some "Honda" -> Some Honda
        | Some "Suzuki" -> Some Suzuki
        | Some "Kawasaki" -> Some Kawasaki
        | Some "Yamaha" -> Some Yamaha
        | Some _ -> Some Unknown
        | None -> Some Unknown

    let ManufacturerFromLetter (letter : string) : Manufacturer option =
         match letter with
            |  "H" -> Some Honda
            |  "S" -> Some Suzuki
            |  "K" -> Some Kawasaki
            |  "Y" -> Some Yamaha
            |  _ -> Some Unknown
    let ManufacturerToString (m :Manufacturer) =
        match m with
        | Honda -> "Honda"
        | Kawasaki -> "Kawasaki"
        | Yamaha -> "Yamaha"
        | Suzuki -> "Suzuki"
        | Unknown -> "Unknown"
