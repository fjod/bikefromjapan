module FsDomain.Interfaces

open System
open System.Threading.Tasks
open FsTypes

type IBikeRepository =
    abstract AddBikes: bikes : System.Collections.Generic.List<Bike> -> Task  
    abstract GetBikeModels : manLetter : string -> Task<DbResult<System.Collections.Generic.List<BikeModel>>>    
    abstract GetBikeModels : unit -> Task<DbResult<System.Collections.Generic.List<BikeModel>>>    
    abstract GetBikes: startYear : int * stopYear : int * manLetter : string * page : int -> Task<System.Collections.Generic.List<Bike>>
    abstract GetBikes: manLetter : string * page : int -> Task<System.Collections.Generic.List<Bike>>
    abstract GetBikesFromYear: startYear : int * manLetter : string * page : int -> Task<System.Collections.Generic.List<Bike>>
    abstract GetBikesBeforeYear: stopYear : int * manLetter : string * page : int -> Task<System.Collections.Generic.List<Bike>>
    abstract GetBikes: models : int list  -> Task<System.Collections.Generic.List<Bike>>

type IUserRepository =
    abstract CreateUser: email : string * password : string * salt : string -> Task<DbResult<string>>
    abstract DeleteUser : email : string -> Task<DbResult<string>>
    abstract GetUserWithoutBikes : email : string -> Task<DbResult<User>>    
    //Все загруженные аукционные данные для избранных моделей
    abstract GetAuctionDataForUser : email : string -> Task<System.Collections.Generic.List<Bike>>    
    abstract GetUserBikeModels : email : string -> Task<System.Collections.Generic.List<BikeModel>>    
    abstract AddBikesToUser: email : string * bikes : System.Collections.Generic.List<int> -> Task
    // Аукционные данные за последние 23 часа для избранных моделей пользователей 
    abstract GetAuctDataForUsers : unit -> Task<System.Collections.Generic.List<UserAuctData>>
