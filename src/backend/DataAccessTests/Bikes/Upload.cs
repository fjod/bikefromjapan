using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccessTests.Bikes;

public class Upload : BikesTable
{
    /*
     * https://docs.microsoft.com/en-us/ef/core/testing/testing-without-the-database#sqlite-in-memory
     * https://stackoverflow.com/questions/57483905/how-to-run-xunit-in-parallel-without-collision-in-dbcontext-primary-key
     */
    public Upload(BikesListFixture fixture) : base(fixture)
    {
    }

    [Fact]
    public async Task Test()
    {
        await BikesRepository.AddBikes(Fixture.Data);
        var count = await BikesContext.Bikes.CountAsync();
        count.Should().Be(57);
        var aucData = await BikesContext.AuctionData.ToListAsync();
        foreach (var auctionData in aucData)
        {
            auctionData.Bike.Should().NotBeNull();
        }

        var bikeModels = await BikesRepository.GetBikeModels("H");
        bikeModels.Should().NotBeNull();
        bikeModels.Payload.Should().HaveCount(57);
    }
}