using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace DataAccessTests.Bikes;

public class AllModels : BikesTable
{
   public AllModels(BikesListFixture fixture) : base(fixture)
   {
   }
   
   [Fact]
   public async Task Test()
   {
      await BikesRepository.AddBikes(Fixture.Data);
      var models = await BikesRepository.GetBikeModels();
      models.Error.Should().Be(String.Empty);
      models.Payload.Select(b => b.Manufacturer).Distinct().Should().HaveCount(1);
   }
}