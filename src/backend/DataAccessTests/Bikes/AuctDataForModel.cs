using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.FSharp.Collections;
using Xunit;

namespace DataAccessTests.Bikes;

public class AuctDataForModel : BikesTable
{
    public AuctDataForModel(BikesListFixture fixture) : base(fixture)
    {
    }
    
    [Fact]
    public async Task Test()
    {
        await BikesRepository.AddBikes(Fixture.Data);
        var data = await BikesRepository.GetBikeModels();

        var CS_list = new List<int> {data.Payload[0].Id, data.Payload[2].Id};
        var bikes1 = await BikesRepository.GetBikes(ListModule.OfSeq(CS_list));
        Assert.NotEmpty(bikes1);
        bikes1.Should().HaveCount(4);
        
        CS_list = new List<int> {data.Payload[3].Id, data.Payload[5].Id};
        var bikes2 = await BikesRepository.GetBikes(ListModule.OfSeq(CS_list));
        Assert.NotEmpty(bikes2);
        bikes2.Should().HaveCount(7);
    }
}