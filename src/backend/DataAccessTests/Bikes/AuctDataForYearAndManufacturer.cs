
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace DataAccessTests.Bikes;

public class AuctDataForYearAndManufacturer : BikesTable
{
    public AuctDataForYearAndManufacturer(BikesListFixture fixture) : base(fixture)
    {
    }
    
    [Fact]
    public async Task Test()
    {
        await BikesRepository.AddBikes(Fixture.Data);
        var data = await BikesRepository.GetBikes(1900, 2020, "H", 0);
        Assert.NotEmpty(data);
        data.Should().HaveCount(10);
        
        data = await BikesRepository.GetBikes(2005, 2020, "H", 0);
        Assert.NotEmpty(data);
        data.Should().HaveCount(10);
    }
}