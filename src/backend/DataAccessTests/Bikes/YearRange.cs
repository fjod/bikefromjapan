using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Domain.Objects;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace DataAccessTests.Bikes;

public class YearRange : BikesTable
{
    public YearRange(BikesListFixture fixture) : base(fixture)
    {
    }
    
    [Fact]
    public async Task ForSameBikeModel()
    {
        var bike = (int year) => Generate("H", "test1", year);
        var range = Enumerable.Range(2000, 5).Select(bike);
        await BikesRepository.AddBikes(range.ToList());
        var auctDataCount = await BikesContext.AuctionData!.CountAsync();
        auctDataCount.Should().Be(5);
        var model = await BikesContext.Bikes.ToListAsync();
        model.Should().NotBeEmpty().And.HaveCount(1);
        model[0].StartYear.Should().Be(2000);
        model[0].StopYear.Should().Be(2004);
    }
}