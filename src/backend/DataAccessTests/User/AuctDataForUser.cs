using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace DataAccessTests.User;

public class AuctDataForUser : BikesAndUsersTables
{
    public AuctDataForUser(BikesListFixture fixture) : base(fixture)
    {
    }

    [Fact]
    public async Task GetFromFixture()
    {
        string email = "email@gg.com";
        string email2 = "email2@gg.com";

        await BikesRepository.AddBikes(Fixture.Data);

        await UserRepository.CreateUser(email, "pword", "salt");
        await UserRepository.AddBikesToUser(email, new List<int> {1});

        await UserRepository.CreateUser(email2, "pword2", "salt");
        await UserRepository.AddBikesToUser(email2, new List<int> {3});

        var userBikes = await UserRepository.GetAuctDataForUsers();
        Assert.Equal(2, userBikes[0].Bikes.Count);
        Assert.Equal(2, userBikes[1].Bikes.Count);

        List<FsTypes.Bike> addToday = new List<FsTypes.Bike>();
        for (int i = 0; i < 2; i++)
        {
            addToday.Add(Generate(FsTypes.Manufacturer.Honda, userBikes[0].Bikes[0].Model));
            addToday.Add(Generate(FsTypes.Manufacturer.Honda, userBikes[0].Bikes[1].Model));
            addToday.Add(Generate(FsTypes.Manufacturer.Honda, userBikes[1].Bikes[0].Model));
        }

        await BikesRepository.AddBikes(addToday);
        
        userBikes = await UserRepository.GetAuctDataForUsers();
        Assert.Equal(6, userBikes[0].Bikes.Count);
        Assert.Equal(4, userBikes[1].Bikes.Count);
    }

    private Random rand = new Random();
    private FsTypes.Bike Generate(FsTypes.Manufacturer manufacturer, string name)
    {
        string temp = "";
        for (int i = 0; i < 5; i++)
        {
            temp += rand.NextInt64(1,10);
        }

        return new FsTypes.Bike(manufacturer, name, (int)rand.NextInt64(1990, 2020), $"key{temp}", (int)rand.NextInt64(10000, 200000), $"img{temp}");
    }
}