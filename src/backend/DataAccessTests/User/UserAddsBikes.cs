using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace DataAccessTests.User;

public class UserAddBikes : BikesAndUsersTables
{
    public UserAddBikes(BikesListFixture fixture) : base(fixture)
    {
    }

    [Fact]
    public async Task Works()
    {
        string email = "email@gg.com";
        
        await BikesRepository.AddBikes(Fixture.Data);
        await UserRepository.CreateUser(email, "pword", "salt");

        await UserRepository.AddBikesToUser(email, new List<int>{ 0, 1});

        var userBikes = await UserRepository.GetAuctionDataForUser(email);
        Assert.NotNull(userBikes);
        Assert.Contains(userBikes, b => b.Model.Contains("CB1300SU-PA-BORUDO-RU"));
        Assert.Equal(2, userBikes.Count);

        var userModels = await UserRepository.GetUserBikeModels(email);
        Assert.NotNull(userModels);
        Assert.Contains(userModels, b => b.Model.Contains("CB1300SU-PA-BORUDO-RU"));
        Assert.Single(userModels);
    }
}