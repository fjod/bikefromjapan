using DataAccess.Repositories;

namespace DataAccessTests;

public class BikesAndUsersTables : InMemoryContext
{
    protected BikesRepository BikesRepository { get; }
    protected UserRepository UserRepository { get; }
    public BikesAndUsersTables(BikesListFixture fixture) : base(fixture)
    {
        BikesContext.Database.EnsureCreated();
        BikesRepository = new BikesRepository(BikesContext);
        UserRepository = new UserRepository(BikesContext);
    }
}