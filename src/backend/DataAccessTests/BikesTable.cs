using System;
using DataAccess.Repositories;

namespace DataAccessTests;

public class BikesTable : InMemoryContext
{
    protected BikesRepository BikesRepository { get; }
    protected BikesTable(BikesListFixture fixture) : base(fixture)
    {
        BikesContext.Database.EnsureCreated();
        BikesRepository = new BikesRepository(BikesContext);
    }

    protected static FsTypes.Bike Generate(string manufacturerLetter, string model, int year)
    {
        return new FsTypes.Bike
        (
            FsTypes.ManufacturerHelper.ManufacturerFromLetter(manufacturerLetter).Value,
            model,
            year,
            String.Empty, 
            10,
            String.Empty
        );
    }
}