using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Sdk;

namespace DataAccessTests;

public abstract class InMemoryContext : IClassFixture<BikesListFixture>
{
    protected BikesListFixture Fixture { get; }
    protected BikesContext BikesContext { get; }
    protected InMemoryContext(BikesListFixture fixture)
    {
        Fixture = fixture;
        
        var options = new DbContextOptionsBuilder<BikesContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;
        BikesContext = new BikesContext(options);
    }
}

public class BikesListFixture : IDisposable
{
    private static List<FsTypes.Bike>? _data;
    public List<FsTypes.Bike> Data { get; }
    public BikesListFixture()
    {
        if (_data == null)
        {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            path = Path.Combine(path!, "japBikesFromAzure.json");
            var ret = JsonSerializer.Deserialize<List<TempBike>>(File.OpenRead(path));
            if (ret == null)
            {
                throw new NullException("Bikes list from json is null for some reason");
            }

            Data = ret.Select(b =>
                new FsTypes.Bike(
                    FromString(b.manufacturer),
                    b.model.Replace(" ", "").ToUpperInvariant(), b.year, b.key, b.mileage, b.image)).ToList();
            _data ??= Data;
        }
        else
        {
            Data = _data;
        }
    }

    public void Dispose()
    {  
    }
    
    FsTypes.Manufacturer? FromString(string m)
    {
        var convert = FsTypes.ManufacturerHelper.ManufacturerFromLetter(m);
        try
        {
            return convert.Value;
        }
        catch (NullReferenceException)
        {
            return null;
        }
    }

    private record TempBike(string model, int year, string key, int mileage, string image, string manufacturer);
}

