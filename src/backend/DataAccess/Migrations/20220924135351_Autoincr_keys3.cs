﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class Autoincr_keys3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserBikes_Bikes_BikesId",
                table: "UserBikes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserBikes_Users_UsersId",
                table: "UserBikes");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Users",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "UsersId",
                table: "UserBikes",
                newName: "UsersUserId");

            migrationBuilder.RenameColumn(
                name: "BikesId",
                table: "UserBikes",
                newName: "BikesBikeId");

            migrationBuilder.RenameIndex(
                name: "IX_UserBikes_UsersId",
                table: "UserBikes",
                newName: "IX_UserBikes_UsersUserId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Manufacturers",
                newName: "ManufacturerId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Bikes",
                newName: "BikeId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "AuctionData",
                newName: "AuctionId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserBikes_Bikes_BikesBikeId",
                table: "UserBikes",
                column: "BikesBikeId",
                principalTable: "Bikes",
                principalColumn: "BikeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserBikes_Users_UsersUserId",
                table: "UserBikes",
                column: "UsersUserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserBikes_Bikes_BikesBikeId",
                table: "UserBikes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserBikes_Users_UsersUserId",
                table: "UserBikes");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Users",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "UsersUserId",
                table: "UserBikes",
                newName: "UsersId");

            migrationBuilder.RenameColumn(
                name: "BikesBikeId",
                table: "UserBikes",
                newName: "BikesId");

            migrationBuilder.RenameIndex(
                name: "IX_UserBikes_UsersUserId",
                table: "UserBikes",
                newName: "IX_UserBikes_UsersId");

            migrationBuilder.RenameColumn(
                name: "ManufacturerId",
                table: "Manufacturers",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "BikeId",
                table: "Bikes",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "AuctionId",
                table: "AuctionData",
                newName: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserBikes_Bikes_BikesId",
                table: "UserBikes",
                column: "BikesId",
                principalTable: "Bikes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserBikes_Users_UsersId",
                table: "UserBikes",
                column: "UsersId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
