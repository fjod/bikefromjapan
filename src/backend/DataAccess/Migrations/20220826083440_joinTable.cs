﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class joinTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bikes_Users_UserId",
                table: "Bikes");

            migrationBuilder.DropIndex(
                name: "IX_Bikes_UserId",
                table: "Bikes");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Bikes");

            migrationBuilder.CreateTable(
                name: "UserBikes",
                columns: table => new
                {
                    BikesId = table.Column<int>(type: "int", nullable: false),
                    UsersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBikes", x => new { x.BikesId, x.UsersId });
                    table.ForeignKey(
                        name: "FK_UserBikes_Bikes_BikesId",
                        column: x => x.BikesId,
                        principalTable: "Bikes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBikes_Users_UsersId",
                        column: x => x.UsersId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_UserBikes_UsersId",
                table: "UserBikes",
                column: "UsersId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserBikes");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Bikes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bikes_UserId",
                table: "Bikes",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bikes_Users_UserId",
                table: "Bikes",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
