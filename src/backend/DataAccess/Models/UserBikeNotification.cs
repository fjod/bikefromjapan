namespace DataAccess.Models;

public class UserBikeNotification
{
    public User User { get; set; }
    
    public AuctionData AuctionData { get; set; }
    
    public DateTime SentStamp { get; set; }
}