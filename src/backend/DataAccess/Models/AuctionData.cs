
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Models;

public class AuctionData
{
    public int AuctionId { get; set; }
    
    [ForeignKey("BikeId")]
    public Bike Bike { get; set; }
    public int Mileage { get; set; }
    public DateTime ScrapedAt { get; set; }
    public string Image { get; set; }
    public string Key { get; set; }
    public int Year { get; set; }

    public FsTypes.Bike Create()
    {
        return new FsTypes.Bike(FsTypes.ManufacturerHelper.ManufacturerFromString(Bike.Manufacturer.Name).Value, Bike.Name, Year, Key,Mileage,Image);
    }
    
    public FsTypes.Bike Create(FsTypes.Manufacturer manufacturer)
    {
        return new FsTypes.Bike(manufacturer, Bike.Name, Year, Key,Mileage,Image);
    }
}