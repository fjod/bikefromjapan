namespace DataAccess.Models;

public class Manufacturer
{
    
    public int ManufacturerId { get; set; }
    public string Name { get; set; } = string.Empty;
}