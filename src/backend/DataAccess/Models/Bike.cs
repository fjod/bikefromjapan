using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models;

public class Bike
{
    [Key]
    public int BikeId { get; set; }
    public Manufacturer Manufacturer { get; set; }
    public string Name { get; set; } = string.Empty;
    public int StartYear { get; set; }
    public int StopYear { get; set; }
    
    public ICollection<User> Users { get; set; }

    public FsTypes.BikeModel Create()
    {
        return new FsTypes.BikeModel(FsTypes.ManufacturerHelper.ManufacturerFromString(Manufacturer.Name).Value, Name, StartYear,StopYear, BikeId);
    }
}