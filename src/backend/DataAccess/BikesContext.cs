﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess;

//\\wsl$\docker-desktop-data\version-pack-data\community\docker\volumes\Bikes  - volume
// pass 312312
// port 3306
// docker run --name bikes_mysql -p 3306:3306 -v /var/lib/docker/volumes/Bikes:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=312312 -d mysql:latest
// user = root
public class BikesContext : DbContext
{
    public BikesContext()
    {
        // ef cli ctor
    }
    
    public BikesContext(DbContextOptions options) : base(options)
    {
    }

    /// <summary>
    /// Bike data from auction (one model can have several auction offers)
    /// </summary>
    public DbSet<AuctionData> AuctionData { get; set; }
    /// <summary>
    /// Bike models
    /// </summary>
    public DbSet<Bike> Bikes { get; set; }
    public DbSet<Manufacturer> Manufacturers { get; set;}
    public DbSet<User> Users { get; set; }
    
    public int PageLength => 10;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Manufacturer>(en => en.HasKey(e => e.ManufacturerId));
        modelBuilder.Entity<Manufacturer>().Property(f => f.ManufacturerId).ValueGeneratedOnAdd();
        
        modelBuilder.Entity<Bike>(en => en.HasKey(e => e.BikeId));
        modelBuilder.Entity<Bike>().Property(f => f.BikeId).ValueGeneratedOnAdd();
        
        modelBuilder.Entity<User>(en => en.HasKey(e => e.UserId));
        modelBuilder.Entity<User>().Property(f => f.UserId).ValueGeneratedOnAdd();

        modelBuilder.Entity<AuctionData>(en => en.HasKey(e => e.AuctionId));
        modelBuilder.Entity<AuctionData>().Property(f => f.AuctionId).ValueGeneratedOnAdd();
        modelBuilder.Entity<AuctionData>().HasOne(b => b.Bike);

        modelBuilder
            .Entity<Bike>()
            .HasMany(p => p.Users)
            .WithMany(p => p.Bikes)
            .UsingEntity(j => j.ToTable("UserBikes"));
        
        modelBuilder.Entity<Manufacturer>().HasData(
            new Manufacturer { ManufacturerId = 1, Name = "Honda"},
            new Manufacturer { ManufacturerId = 2, Name = "Suzuki"},
            new Manufacturer { ManufacturerId = 3, Name = "Kawasaki"},
            new Manufacturer { ManufacturerId = 4, Name = "Yamaha"},
            new Manufacturer { ManufacturerId = 5, Name = "Unknown"}
        );
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        // "server=localhost;database=bikes;user=root;password=312312"
        // old code for cli ef
        if (optionsBuilder == null) throw new ArgumentNullException(nameof(optionsBuilder));
        if (!optionsBuilder.IsConfigured) // in memory is preconfigured for tests
        {
            var connectionString = "server=localhost;database=bikes;user=root;password=312312";
            var serverVersion = new MySqlServerVersion(new Version(8, 0, 27));
            optionsBuilder.UseMySql(connectionString, serverVersion);
        }
    }
}