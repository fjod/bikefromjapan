using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DataAccess.Repositories;

public static class Extensions
{
    public static IIncludableQueryable<Bike, Manufacturer> BikesWithManufacturers(this DbSet<Bike> bikes)
    {
        return bikes.Include(b => b.Manufacturer);
    }
    
    public static Task<List<AuctionData>> AuctionDataForBikes(this IQueryable<Bike> bikesWithManufacturers, BikesContext context, int page)
    {
        return bikesWithManufacturers
            .Join(context.AuctionData, bike => bike, data => data.Bike, (bike, data) => data)
            .Include(b => b.Bike)
            .ThenInclude(b => b.Manufacturer)
            .Skip(page*context.PageLength)
            .Take(context.PageLength)
            .AsNoTracking()
            .ToListAsync();
    }
}