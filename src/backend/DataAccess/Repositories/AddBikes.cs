using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories;

public partial class BikesRepository
{
    public async Task AddBikes(List<FsTypes.Bike> bikes)
    {
        var currentDbBikes = await _context.Bikes.ToListAsync();
        var newBikes = new List<Bike>();
        var currentAuctionDataKeys = await _context.AuctionData
            .Select(b => b.Key)
            .ToListAsync();
        var manufacturers = await GetManufacturers();

        var bikesByModel = bikes.GroupBy(b => b.Model);
        foreach (var bikesWithSameModel in bikesByModel)
        {
            ProcessBikesInGroup(bikesWithSameModel, currentAuctionDataKeys, currentDbBikes, newBikes, manufacturers);
        }

        await _context.SaveChangesAsync();
    }

    /// <summary>
    /// Обработка данных с одинаковыми названиями моделей
    /// </summary>
    /// <param name="modelGroup">Группа</param>
    /// <param name="currentAuctionDataKeys">Идентификаторы уже имеющихся записей аукциона</param>
    /// <param name="currentDbBikes">Список для добавления моделей</param>
    /// <param name="newBikes">Список для сохранения новых найденных моделей</param>
    /// <param name="manufacturers">Производители</param>
    private void ProcessBikesInGroup(IGrouping<string, FsTypes.Bike> modelGroup, List<string> currentAuctionDataKeys, List<Bike> currentDbBikes,
        List<Bike> newBikes, List<Manufacturer> manufacturers)
    {
        foreach (var outputBike in modelGroup)
        {
            if (currentAuctionDataKeys.Contains(outputBike.Key)) continue;

            var startYear = modelGroup.Min(b => b.Year);
            var stopYear = modelGroup.Max(b => b.Year);

            var dbBike = currentDbBikes.FirstOrDefault(b => b.Name.Trim() == outputBike.Model.Trim());
            var newBike = newBikes.FirstOrDefault(b => b.Name.Trim() == outputBike.Model.Trim());
            if (dbBike == null && newBike == null)
            {
                // это новая модель
                AddNewModelAndAuctionData(manufacturers, outputBike, startYear, stopYear, newBikes);
                continue;
            }

            AddAuctDataToOldModel(dbBike, startYear, stopYear, newBike, outputBike);
        }
    }

    /// <summary>
    /// Добавить новую запись аукциона к модели
    /// </summary>
    /// <param name="dbBike">Модель из БД</param>
    /// <param name="startYear">Год новой модели</param>
    /// <param name="stopYear">Год новой модели</param>
    /// <param name="newBike">Новая модель (раньше не было в БД)</param>
    /// <param name="outputBike">Данные аукциона</param>
    /// <exception cref="InvalidOperationException"></exception>
    private void AddAuctDataToOldModel(Bike? dbBike, int startYear, int stopYear, Bike? newBike, FsTypes.Bike outputBike)
    {
        // модель есть в БД
        if (dbBike != null)
        {
            // проверить start/end year
            if (dbBike.StartYear > startYear) dbBike.StartYear = startYear;
            if (dbBike.StopYear < stopYear) dbBike.StopYear = stopYear;
        }

        var newAuctionData = new AuctionData
        {
            Bike = (dbBike ?? newBike) ?? throw new InvalidOperationException("Both bikes are null"),
            Image = outputBike.Image,
            Key = outputBike.Key,
            Mileage = outputBike.Mileage,
            ScrapedAt = DateTime.Now,
            Year = outputBike.Year
        };
        _context.AuctionData.Add(newAuctionData);
    }

    /// <summary>
    /// Добавить новую модель и первую аукционную запись для неё
    /// </summary>
    /// <param name="manufacturers">Список производителей</param>
    /// <param name="outputBike">Модель мотоцикла из рассматриваемой записи</param>
    /// <param name="startYear">Год модели</param>
    /// <param name="stopYear">Год модели</param>
    /// <param name="newBikes">Коллекция новых моделей для обновления</param>
    private void AddNewModelAndAuctionData(List<Manufacturer> manufacturers, FsTypes.Bike outputBike, int startYear, int stopYear, List<Bike> newBikes)
    {
        var thisBikeMan =
            manufacturers.FirstOrDefault(m =>
                m.Name[0] == FsTypes.ManufacturerHelper.ManufacturerToString(outputBike.Manufacturer)[0]);
        if (thisBikeMan != null) // Проверка производителя, вдруг он пришел некорректным
        {
            // Просто добавляем новую модель в БД
            var addBike = new Bike
            {
                Manufacturer = thisBikeMan,
                Name = outputBike.Model,
                StartYear = startYear, StopYear = stopYear
            };
            _context.Bikes.Add(addBike);
            newBikes.Add(addBike);

            var newAuctionData = new AuctionData
            {
                Bike = addBike,
                Image = outputBike.Image,
                Key = outputBike.Key,
                Mileage = outputBike.Mileage,
                ScrapedAt = DateTime.Now,
                Year = outputBike.Year
            };
            _context.AuctionData.Add(newAuctionData);
        }
    }
}