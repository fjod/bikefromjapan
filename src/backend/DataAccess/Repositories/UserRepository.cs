using DataAccess.Models;
using FsDomain;
using Microsoft.EntityFrameworkCore;
using Microsoft.FSharp.Core;

namespace DataAccess.Repositories;

public class UserRepository : Interfaces.IUserRepository
{
    private readonly BikesContext _context;
    private readonly FsTypes.DbResult<string> _okResult = new (String.Empty, String.Empty);
    private readonly FsTypes.User _emptyUser = new (String.Empty, String.Empty, String.Empty);
    
    public UserRepository(BikesContext context)
    {
        _context = context;
        _context.Database.EnsureCreated();
    }
    public async Task<FsTypes.DbResult<string>> CreateUser(string email, string password, string salt)
    {
        var sameEmail = await _context.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Email == email);
        if (sameEmail != null)
        {
            return new FsTypes.DbResult<string>(String.Empty,$"User with email {email} is already registered");
        }
        await _context.Users.AddAsync(new User{Email = email, Password = password, Salt = salt});
        await _context.SaveChangesAsync();

        return _okResult;
    }

    public async Task<FsTypes.DbResult<string>> DeleteUser(string email)
    {
        var users = await _context.Users.Where(u => u.Email == email).ToListAsync();
        if (users.Any())
        {
            _context.RemoveRange(users);
            await _context.SaveChangesAsync();
        }

        return _okResult;
    }

    private async Task<User> GetUserWithBikes(string email)
    {
       return await _context.Users
            .Where(u => u.Email == email)
            .Include(u => u.Bikes)
            .FirstAsync();
    }

    public async Task<FsTypes.DbResult<FsTypes.User>> GetUserWithoutBikes(string email)
    {
        var user = await _context.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Email == email);
        if (user == null)
        {
            return new FsTypes.DbResult<FsTypes.User>(_emptyUser, $"Can not find user for email {email}");
        }

        return new FsTypes.DbResult<FsTypes.User>(new FsTypes.User(email, user.Password, user.Salt), String.Empty);
    }

    public async Task<List<FsTypes.Bike>> GetAuctionDataForUser(string email)
    {
        var user = await GetUserWithBikes(email);

        var userBikesIds = user.Bikes.Select(b => b.BikeId);

        var bikes = await _context.AuctionData.Include(b => b.Bike)
            .ThenInclude(m => m.Manufacturer)
            .Where(b => userBikesIds.Contains(b.Bike.BikeId))
            .AsNoTracking()
            .ToListAsync();

        var a = bikes.Select(z => z.Create());
        return a.ToList();
    }

    public async Task<List<FsTypes.BikeModel>> GetUserBikeModels(string email)
    {
        var user = await _context.Users
            .Where(u => u.Email == email)
            .Include(u => u.Bikes)
            .ThenInclude(b => b.Manufacturer)
            .AsNoTracking()
            .FirstAsync();

        var bikeModels = user.Bikes.Select(b => b.Create());
        return new List<FsTypes.BikeModel>(bikeModels.ToList());
    }

    public async Task AddBikesToUser(string email, List<int> newBikes)
    {
        var user = await GetUserWithBikes(email);

        var bikes = await _context.Bikes.Where(bike => newBikes.Contains(bike.BikeId)).ToListAsync();
        user.Bikes.Clear();
        foreach (var bike in bikes)
        {
            user.Bikes.Add(bike);
        }

        await _context.SaveChangesAsync();
    }

    public async Task<List<FsTypes.UserAuctData>> GetAuctDataForUsers()
    {
        List<FsTypes.UserAuctData> ret = new List<FsTypes.UserAuctData>();

        // Аукционные данные за последние 23 часа для моделей, которые пользователи добавили в "избранное"
        var auctData = await _context.Users
            .Include(u => u.Bikes)
            .SelectMany(b => b.Bikes)
            .Join(_context.AuctionData, bike => bike, auctionData => auctionData.Bike, (user, auctionData) => auctionData)
            .Where(d => d.ScrapedAt - DateTime.Now < TimeSpan.FromHours(23))
            .ToListAsync();

        // Пользователи и их модели в "избранном"
        var usersWithBikes = await _context.Users
            .Include(u => u.Bikes)
            .ThenInclude(b => b.Manufacturer)
            .Select(u => new {u.Email, u.Bikes})
            .ToListAsync();
        
        foreach (var usersWithBike in usersWithBikes)
        {
            var userBikes = usersWithBike.Bikes
                .Join(auctData, bike => bike, data => data.Bike, (bike, data) => (bike, data))
                .Select(d => new FsTypes.Bike(FromString(d.bike.Manufacturer.Name), d.bike.Name, d.data.Year, d.data.Key, d.data.Mileage, d.data.Image));
            ret.Add(new FsTypes.UserAuctData(usersWithBike.Email, userBikes.ToList()));
        }

        return ret;
    }

    FsTypes.Manufacturer? FromString(string m)
    {
        var convert = FsTypes.ManufacturerHelper.ManufacturerFromString(new FSharpOption<string>(m));
        try
        {
            return convert.Value;
        }
        catch (NullReferenceException)
        {
            return null;
        }
    }
}