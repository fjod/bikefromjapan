using DataAccess.Models;
using FsDomain;
using Microsoft.EntityFrameworkCore;
using Microsoft.FSharp.Collections;

namespace DataAccess.Repositories;

public partial class BikesRepository : Interfaces.IBikeRepository
{
    private readonly BikesContext _context;

    public BikesRepository(BikesContext context)
    {
        _context = context;
        _context.Database.EnsureCreated();
    }

    private List<Manufacturer>? _cachedDbManufacturers;

    private async Task<List<Manufacturer>> GetManufacturers()
    {
        return _cachedDbManufacturers ??= await _context.Manufacturers.ToListAsync();
    }
    
    private readonly Dictionary<string, FsTypes.Manufacturer> _cachedFsManufacturers = new Dictionary<string, FsTypes.Manufacturer>(5);
    private FsTypes.Manufacturer FsManufacturerFromLetter(string letter)
    {
        if (_cachedFsManufacturers.ContainsKey(letter)) return _cachedFsManufacturers[letter];
            
        var manufacturer = FsTypes.ManufacturerHelper.ManufacturerFromLetter(letter).Value;
        _cachedFsManufacturers.Add(letter,manufacturer);
        return manufacturer;
    }

    private async Task<Manufacturer> DbManufacturerFromLetter(string letter)
    {
        var manufacturers = await GetManufacturers();
        return manufacturers.First(m => m.Name.StartsWith(letter));
    }

    public async Task<FsTypes.DbResult<List<FsTypes.BikeModel>>> GetBikeModels(string manLetter)
    {
        var man = await DbManufacturerFromLetter(manLetter);

        var bikes = await _context.Bikes
            .Where(b => b.Manufacturer == man)
            .Include(b => b.Manufacturer)
            .AsNoTracking()
            .ToListAsync();

        var ret = bikes.Select(b => b.Create());
        return new FsTypes.DbResult<List<FsTypes.BikeModel>>(ret.ToList(), String.Empty);
    }

    public async Task<FsTypes.DbResult<List<FsTypes.BikeModel>>> GetBikeModels()
    {
        var bikes = await _context.Bikes
            .Include(bike => bike.Manufacturer)
            .AsNoTracking()
            .ToListAsync();

        var ret = bikes.Select(b => b.Create());
        return new FsTypes.DbResult<List<FsTypes.BikeModel>>(ret.ToList(), String.Empty);
    }

    
    public async Task<List<FsTypes.Bike>> GetBikes(int startYear, int stopYear, string manLetter, int page)
    {
        var man = await DbManufacturerFromLetter(manLetter);
        var ret = await _context.Bikes.BikesWithManufacturers()
            .Where(b => b.StartYear >= startYear && b.StopYear <= stopYear && b.Manufacturer == man)
            .AuctionDataForBikes(_context, page);

        return ret.Select(w => w.Create()).ToList();
    }

    public async Task<List<FsTypes.Bike>> GetBikes(string manLetter, int page)
    {
        var dbMan = await DbManufacturerFromLetter(manLetter);
        var fsMan = FsManufacturerFromLetter(manLetter);
        var ret = await _context.Bikes.BikesWithManufacturers()
            .Where(b => b.Manufacturer == dbMan)
            .AuctionDataForBikes(_context, page);

        return ret.Select(w => w.Create(fsMan)).ToList();
    }

    public async Task<List<FsTypes.Bike>> GetBikesFromYear(int startYear, string manLetter, int page)
    {
        var dbMan = await DbManufacturerFromLetter(manLetter);
        var fsMan = FsManufacturerFromLetter(manLetter);
        var ret = await _context.Bikes.BikesWithManufacturers()
            .Where(b => b.StartYear >= startYear && b.Manufacturer == dbMan)
            .AuctionDataForBikes(_context, page);

        return ret.Select(w => w.Create(fsMan)).ToList();
    }

   
    public async Task<List<FsTypes.Bike>> GetBikesBeforeYear(int stopYear, string manLetter, int page)
    {
        var dbMan = await DbManufacturerFromLetter(manLetter);
        var fsMan = FsManufacturerFromLetter(manLetter);
        var ret = await _context.Bikes.BikesWithManufacturers()
            .Where(b => b.StopYear <= stopYear && b.Manufacturer == dbMan)
            .AuctionDataForBikes(_context, page);

        return ret.Select(w => w.Create(fsMan)).ToList();
    }

    public async Task<List<FsTypes.Bike>> GetBikes(FSharpList<int> models)
    {
        var t = await _context.AuctionData
            .Include(a => a.Bike)
            .ThenInclude(z => z.Manufacturer)
            .Where(a => models.Contains(a.Bike.BikeId))
            .AsNoTracking()
            .ToListAsync();

        return t.Select(w => w.Create()).ToList();
    }
}