module backend.BikeDownloader

open System
open System.IO
open System.Net
open System.Net.Http
open System.Text
open System.Text.Json
open Domain.Objects
open FsTypes
open TsInterfaces

let options = JsonSerializerOptions(PropertyNameCaseInsensitive = true)

let deserialize (content:string, manletter:string) =
    if String.IsNullOrEmpty(content)
        then []
    else    
        try        
            let input = JsonSerializer.Deserialize<BikesRoot>(content, options)
            let convertImage (im:string) =
                if (im.StartsWith "https")
                 then im
                 else $"https://projapan.ru{im}"
                
            
            let mapBike (b:APIBike) =
                         let imageIsNotNull = b.preview_image = null |> not
                         match (b.year.HasValue, b.mileage.HasValue, imageIsNotNull) with                 
                            | true, true, true ->
                                             let man = ManufacturerHelper.ManufacturerFromLetter manletter
                                             let mileageValue = b.mileage.Value > 0
                                             match man, mileageValue with
                                                 | Some m, true ->
                                                              Some {
                                                              Manufacturer = m
                                                              Model = b.model.Replace(" ", "").ToUpperInvariant()
                                                              Year = b.year.Value
                                                              Key = b.key
                                                              Mileage = b.mileage.Value
                                                              Image = convertImage b.preview_image
                                                               }
                                                 | _ -> None
                            | _, _, _ ->  None
            Seq.toList input.Bikes |> List.map mapBike |> List.choose id
        with
        | :? ArgumentNullException -> []
        | :? JsonException -> []
        | :? NotSupportedException -> []

type LogFunc =
    string-> string -> string -> unit

type IDownloadBikes =
    abstract Get: string * string * string * LogFunc-> Async<List<Bike>>

let path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
let logPath = System.IO.Path.Combine(path, "BikeDownloadLogs")

let FakeLog _ _ _ =
    ()

let LogInputOutputToFile input output dataName =
    if (Directory.Exists logPath)
        then ()
        else Directory.CreateDirectory logPath |> ignore
    let dt = DateTime.Now
    try
        let inputLogFilename = $"{dataName}-{dt.Date.Day}-{dt.Date.Month}--{dt.Hour}-{dt.Minute}.txt".Trim()        
        File.WriteAllText(Path.Combine(logPath, inputLogFilename), input)
    
        let outputLogFilename = $"{dataName}-{dt.Date.Day}-{dt.Date.Month}--{dt.Hour}-{dt.Minute}.Result.txt".Trim()        
        File.WriteAllText(Path.Combine(logPath, outputLogFilename), output)
    with
       | :? IOException as ex -> Console.WriteLine ex.Message
    ()

let conv (b:Bike) : TS_BikeOutput =
   TS_BikeOutput(
       ManufacturerHelper.ManufacturerToString b.Manufacturer,
       b.Key, b.Model, b.Image, b.Year, b.Mileage)
   

type HttpDownloader() =
    let client = new HttpClient()
    let builder = StringBuilder()
    interface IDownloadBikes with
        member this.Get (startYear : string , endYear : string , manletter : string, log : LogFunc) =
            async {
            builder.Clear()
                   .Append("https://projapan.ru/bikes?mileage_min=0&mileage_max=100&year_min=")
                   .Append(startYear)
                   .Append("&year_max=")
                   .Append(endYear)
                   .Append("&rank_min=0&rank_max=5&auction%5B%5D=auc&auction%5B%5D=bds&auction%5B%5D=jba&auction%5B%5D=arai&date=all&manufacturer=")
                   .Append(manletter) |> ignore

            let message = new HttpRequestMessage (HttpMethod.Get, builder.ToString())
            message.Headers.Add("Host", "projapan.ru")
            message.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json; charset=utf-8")
            message.Headers.Add("Accept", "*/*")
            message.Headers.Add("Connection", "keep-alive")
            let! content = client.SendAsync(message) |> Async.AwaitTask
            let! contentAsString = content.Content.ReadAsStringAsync() |> Async.AwaitTask
            
            let ret = deserialize (contentAsString, manletter)
            let save = ret |> List.map conv
            let outputLogData = JsonSerializer.Serialize(save)
            
            log contentAsString outputLogData $"{startYear}.{endYear}.{manletter}"
            Console.WriteLine $"{DateTime.Now.TimeOfDay}: Got data for {startYear}.{endYear}.{manletter}"
            return ret
            }

