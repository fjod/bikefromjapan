module backend.InterfaceConverters

open FsTypes
open TsInterfaces

let ConvertBike (b : Bike) : TS_BikeOutput =
    TS_BikeOutput(ManufacturerHelper.ManufacturerToString b.Manufacturer,
                              b.Key, b.Model, b.Image, b.Year, b.Mileage)

    
let ConvertBikeModel (b : BikeModel) : TS_BikeModel =
    TS_BikeModel(ManufacturerHelper.ManufacturerToString b.Manufacturer, b.Model, b.StartYear, b.StopYear, b.Id)
    