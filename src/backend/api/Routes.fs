

module backend.Routes

open Microsoft.AspNetCore.Http
open Giraffe
open backend.Api
open backend.Security.Jwt   

    
let webApp :(HttpFunc -> HttpContext -> HttpFuncResult) =
    choose [        
        GET >=>
            choose [
                route "/" >=> htmlFile "wwwroot/index.html"
                route "/ping" >=> text "pong"
                route "/authorizedPing" >=> authorize >=> handleGetSecured
                routef "/bike/%s/%s/%s" bikesHandler
                route "/models" >=> bikesModels
                route "/auctData"  >=> auctDataHandler
                route "/userAuctData" >=> authorize >=> userAuctDataHandler
                route "/userModels" >=> authorize >=> getUserBikesModels
                route "/test" >=> testBikesSaving
                route "/testEmails" >=> testEmails
               ]
        POST >=>
        choose [
                route "/register" >=> registerHandler
                route "/login" >=> loginHandler
                route "/auctData" >=>  auctDataForModels
                route "/addUserModels" >=> authorize >=>   addBikesToUser
            ]
        setStatusCode 404 >=> text "Not Found"]
  