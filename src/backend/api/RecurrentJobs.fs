module backend.RecurrentJob

open System
open System.Text
open FsDomain.Interfaces
open FsTypes
open RestSharp
open RestSharp.Authenticators
open backend.BikeDownloader
open backend.EmailSender

let Download(context:IBikeRepository)(bikes:IDownloadBikes)(timeoutModifier:int) logFunc =
    let rand = Random()
    let createReq ((startYear:int, endYear : int), letter : string) =
        async{
            do! Async.Sleep(rand.Next (10000/timeoutModifier,50000/timeoutModifier))             
            return! bikes.Get(startYear.ToString(), endYear.ToString(), letter, logFunc)
        }    
    async{
        let years = [2000..2..2022] |> List.map (fun b -> (b-1, b))
        let! bikesData = [ "H"; "S"; "K"; "Y"]
                         |> List.map (fun letter -> List.map (fun year -> (year, letter)) years)
                         |> List.collect id
                         |> List.map createReq
                         |> Async.Sequential
        let ret = System.Collections.Generic.List<Bike>()
        Array.iter ret.AddRange bikesData 
        context.AddBikes(ret) |> Async.AwaitTask |> ignore
        ()
    }
    
let createRestClient() =
   let client = new RestClient("https://api.mailgun.net/v3")
   client.Authenticator <- HttpBasicAuthenticator("api","12ec50c7fd639fb3bd933b979a01471e-1b3a03f6-4cee2b75")
   client

let domainName = "sandbox93a274bf1ab64ef2aec11f0e4693ce25.mailgun.org"
let createRestRequest email content =
    let request = new RestRequest ()   
    request.AddParameter ("domain", domainName, ParameterType.UrlSegment) |> ignore
    request.Resource <- $"{domainName}/messages";
    request.AddParameter ("from", $"Excited User <mailgun@{domainName}>") |> ignore
    request.AddParameter ("to", email) |> ignore   
    request.AddParameter ("subject", "Hello from YourBikeFromJapan") |> ignore
    request.AddParameter ("text", content) |> ignore
    request.Method <- Method.Post;
    request
  
let SendEmail(context:IUserRepository) (sender:SendEmail) (log : EmailLog) =
    task{
        let sb = StringBuilder()
        let! data = context.GetAuctDataForUsers()
        for d in data do
            sender d
            sb.Append $"Sent {d.Bikes.Count} to {d.Email}" |> ignore
        log (sb.ToString())
    }
    
    

let TestEmail() =
    let client = createRestClient()
    let request= createRestRequest "fjod10199@gmail.com" "test test test"
    let response = client.Execute request
    response