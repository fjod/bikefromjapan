module backend.Database

open System
open System.Threading.Tasks
open DataAccess.Repositories
open DataAccess
open FsTypes
open Microsoft.EntityFrameworkCore
open TsInterfaces
open FsDomain.Interfaces

let ver = MySqlServerVersion(Version(8, 0, 28))
let options(connString:string) =
    let one = DbContextOptionsBuilder<BikesContext>()
    one.UseMySql(connString, ver).Options    
    
let context connString = new BikesContext(options(connString))
let bikesRepo connString = BikesRepository(context(connString)) :> IBikeRepository
let userRepo connString = UserRepository(context(connString)) :> IUserRepository

let matchResult( res: Task<DbResult<'a>>) =
   task{
       let! input = res
       return match input.Error with
                | "" -> Ok input.Payload
                | error -> Error error
    }

let DB_registerUser (email:string, dbSalt:string, dbPass:string, connstring) =
    task {
    return! userRepo(connstring).CreateUser(email, dbPass, dbSalt) |> matchResult
    }

let DB_getUser (email:string, connstring) =
    task{
    return! userRepo(connstring).GetUserWithoutBikes(email) |> matchResult
    }
    
let DB_getUserModels (email:string, connstring) =
    task{
    return! userRepo(connstring).GetUserBikeModels(email)
    }

let DB_deleteUser (email:string, connstring) =
    task{
    return! userRepo(connstring).DeleteUser(email) |> matchResult
    }

let DB_getUserAuctData (email:string, connstring) =
    task{
    return! userRepo(connstring).GetAuctionDataForUser(email)
    }

let DB_addBikesForUser (email : string, bikes:TS_UserBikeInput, connstring) =
    task{         
        let backendBikes = bikes.BikeIds
        return! userRepo(connstring).AddBikesToUser((email,backendBikes))
    }
    
let DB_getBikeModels (manLetter:string, connstring) =
    task{        
        let ret: DbResult<System.Collections.Generic.List<BikeModel>> =
            { Payload = System.Collections.Generic.List<BikeModel>(); Error = "bad manufacturer letter" }
        let asTask = Task.FromResult ret
        let manufacturer = ManufacturerHelper.ManufacturerFromLetter manLetter
        match manufacturer with
            | Some _ -> return! bikesRepo(connstring).GetBikeModels(manLetter) |> matchResult
            | _ -> return! asTask |> matchResult
    }

let DB_getAllBikeModels connstring =
    task{
        return! bikesRepo(connstring).GetBikeModels()
    }
    
let DB_auctDataFull(startYear:int, stopYear:int, manLetter : string, page: int, connstring) =
    task{
        return! bikesRepo(connstring).GetBikes(startYear, stopYear, manLetter, page)
    }
    
let DB_auctDataFromYear(startYear:int, manLetter : string, page: int, connstring) =
    task{
        return! bikesRepo(connstring).GetBikesFromYear(startYear, manLetter, page)
    }
    
let DB_auctDataUntilYear(stopYear:int, manLetter : string, page: int, connstring) =
    task{
        return! bikesRepo(connstring).GetBikesBeforeYear(stopYear, manLetter, page)
    }

let DB_auctData(manLetter : string, page: int, connstring) =
    task{
        return! bikesRepo(connstring).GetBikes(manLetter, page)
    }
    
let DB_auctDataForModels(models : int list, connstring) =
    task{
        return! bikesRepo(connstring).GetBikes(models)
    }
    
let DB_auctDataForUsers connstring =
    task{
        return! userRepo(connstring).GetAuctDataForUsers()
    }
