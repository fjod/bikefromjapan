module backend.EmailSender

open System
open System.IO
open FsTypes
open RestSharp

type SendEmail =
    UserAuctData -> unit

type EmailLog =
    string -> unit
    
let path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
let logPath = System.IO.Path.Combine(path, "EmailSendLogs")
    
let EmailSender (data :UserAuctData) (client:RestClient) =    
    ()
    
let EmailLogger (data :string ) =    
    if (Directory.Exists logPath)
        then ()
        else Directory.CreateDirectory logPath |> ignore
    let dt = DateTime.Now
    try
        let inputLogFilename = $"EmailLog-{dt.Date.Day}-{dt.Date.Month}--{dt.Hour}-{dt.Minute}.txt".Trim()        
        File.WriteAllText(Path.Combine(logPath, inputLogFilename), data)
    with
       | :? IOException as ex -> Console.WriteLine ex.Message
    ()