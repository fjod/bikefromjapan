﻿module Server.Environment

open System.IO
open System

let (</>) x y = Path.Combine(x, y)

let BikeProjectFolderName = "bikes"

let dataFolder =
    let appDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
    let folder = appDataFolder </> BikeProjectFolderName
    let directoryInfo = DirectoryInfo(folder)
    if not directoryInfo.Exists then Directory.CreateDirectory folder |> ignore
    printfn "Using data folder: %s" folder
    folder
    
let securityTokenFile = dataFolder </> BikeProjectFolderName
