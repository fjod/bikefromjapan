﻿module Server.Security

open System
open System.IO
open System.Security.Cryptography
open System.Text
open System.Text.Json
open DataAccess.Models

let createRandomKey() =
    let generator = System.Security.Cryptography.RandomNumberGenerator.Create()
    let randomKey = Array.init 32 byte
    generator.GetBytes(randomKey)
    randomKey

let private passPhrase =
    let securityTokenFile = FileInfo(Environment.securityTokenFile)
    if not securityTokenFile.Exists then
        let passPhrase = createRandomKey()
        File.WriteAllBytes(securityTokenFile.FullName, passPhrase)
    File.ReadAllBytes(securityTokenFile.FullName)

let private encodeString (payload : string) =
    Jose.JWT.Encode(payload, passPhrase, Jose.JweAlgorithm.A256KW, Jose.JweEncryption.A256CBC_HS512)
let private decodeString (jwt : string) =
    Jose.JWT.Decode(jwt, passPhrase, Jose.JweAlgorithm.A256KW, Jose.JweEncryption.A256CBC_HS512)


let encodeJwt token = JsonSerializer.Serialize token |> encodeString

let decodeJwt (jwt : string) : User =
    decodeString jwt |> JsonSerializer.Deserialize<User>


let validateJwt (jwt : string) =
    try
        let token = decodeJwt jwt
        Ok token
    with _ -> Error "TokenInvalid"

let utf8Bytes (input : string) = Encoding.UTF8.GetBytes(input)
let base64 (input : byte []) = Convert.ToBase64String(input)
let sha256 = SHA256.Create()
let sha256Hash (input : byte []) : byte [] = sha256.ComputeHash(input)
let verifyPassword password saltBase64 hashBase64 =
    let salt = Convert.FromBase64String(saltBase64)    
    let pword = utf8Bytes password
    Array.concat [salt; pword]
                            |> sha256Hash
                            |> base64
                            |> (=) hashBase64


let createUser (email:string, inputPassword:string)=    
    let salt = createRandomKey()
    let password = utf8Bytes inputPassword
    let saltyPassword = Array.concat [ salt; password ]
    let passwordHash = sha256Hash saltyPassword
    let passForDb = base64 passwordHash
    let saltForDb = base64 salt
    (email, saltForDb, passForDb)
    

