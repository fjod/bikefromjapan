module backend.Security.Jwt

open System
open FsTypes
open Giraffe
open System.Text
open System.Security.Claims
open System.IdentityModel.Tokens.Jwt
open Microsoft.AspNetCore.Http
open Microsoft.IdentityModel.Tokens
open Microsoft.AspNetCore.Authentication.JwtBearer
open TsInterfaces
open Server.Security


[<CLIMutable>]
type TokenResult =
    {
        Token : string
    }

let secret = "spadR2dre#u-ruBrE@TepA&*Uf@U"

let authorize: HttpFunc -> HttpContext -> HttpFuncResult =
    requiresAuthentication (challenge JwtBearerDefaults.AuthenticationScheme)

let generateToken email =
    let claims = [|
        Claim(JwtRegisteredClaimNames.Sub, email);
        Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()) |]

    let expires = Nullable(DateTime.UtcNow.AddHours(1.0))
    let notBefore = Nullable(DateTime.UtcNow)
    let securityKey = SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret))
    let signingCredentials = SigningCredentials(key = securityKey, algorithm = SecurityAlgorithms.HmacSha256)

    let token =
        JwtSecurityToken(
            issuer = "jwtwebapp.net",
            audience = "jwtwebapp.net",
            claims = claims,
            expires = expires,
            notBefore = notBefore,
            signingCredentials = signingCredentials)

    let tokenResult = {
        Token = JwtSecurityTokenHandler().WriteToken(token)
    }

    tokenResult
    
let createRegDataOutput regResultWith token =
    match regResultWith with
        | Ok _ ->
            TS_RegistrationOutput("register ok", token)
        | Error e ->
            TS_RegistrationOutput($"register error {e}", String.Empty)

let createLoginOutput (regResultWith : Result<TokenResult,string>)  =
    match regResultWith with
        | Ok e ->
            TS_RegistrationOutput("login ok", e.Token)
        | Error e ->
            TS_RegistrationOutput($"login error {e}", String.Empty)
            

let checkPassword pword (user: Result<User, string>)  =
    match user with
    | Ok u -> verifyPassword pword u.Salt u.Password
    | Error _ -> false
    
let createToken (passwordOk: bool) email =
    match passwordOk with
    | true -> Ok (generateToken email)
    | false -> Error "bad pass"
