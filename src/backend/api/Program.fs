module backend.App

open System
open System.IO
open System.Text
open System.Threading.Tasks
open System.Transactions
open DataAccess
open Hangfire
open Hangfire.Dashboard
open Hangfire.MySql
open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Server.Kestrel.Core
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open Microsoft.IdentityModel.Tokens
open Newtonsoft.Json
open backend.Api
open backend.BikeDownloader
open backend.Security.Jwt
open RecurrentJob
open Database
open Routes
open FsTypes
open Microsoft.EntityFrameworkCore
open Microsoft.AspNetCore.Cors
// https://dsincl12.medium.com/json-web-token-with-giraffe-and-f-4cebe1c3ef3b

    


// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder : CorsPolicyBuilder) =
    builder
        .WithOrigins(
            "http://localhost:3000",
            "https://localhost:3000")
       .AllowAnyMethod()
       .AllowAnyHeader()
       |> ignore

let DownloadJob (connstring) : Task =
    task{
    let repo = bikesRepo(connstring)
    let downloader = HttpDownloader()
    do! Download repo downloader 1 LogInputOutputToFile 
    return  Task.CompletedTask
    }

type HangfireAuthorization () =
    interface IDashboardAuthorizationFilter with
        override _.Authorize (dc: DashboardContext) =
            false

let hangfireDashboardOptions: DashboardOptions =
     let x = DashboardOptions()
     x.Authorization <- [ HangfireAuthorization() ] // or [| HangfireAuthorization() |]
     x

let configureApp (app : IApplicationBuilder) =
    
    
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    app.UseAuthentication() |> ignore
    match env.EnvironmentName with
    | "Test" -> app                  
                   .UseGiraffe(webApp)
    | "Development" ->
                app.UseHangfireDashboard("/hangfire") |> ignore
                app.UseDeveloperExceptionPage()
                   .UseDefaultFiles()
                   .UseStaticFiles()
                   .UseCors(configureCors)
                   .UseGiraffe(webApp)
                let conf = app.ApplicationServices.GetService<IConfiguration>()
                let connStringMysql =  (conf.GetSection "MySqlConnectionString").Value
                RecurringJob.AddOrUpdate((fun () -> Console.WriteLine "Recurring Job"), Cron.Minutely)                
                RecurringJob.AddOrUpdate((fun () -> DownloadJob(connStringMysql) ), "30 13 * * *")
    | _ ->
                
                let conf = app.ApplicationServices.GetService<IConfiguration>()
                let connStringMysql =  (conf.GetSection "MySqlConnectionString").Value
                                
                RecurringJob.AddOrUpdate((fun () -> DownloadJob(connStringMysql) ), "30 13 * * *")
                
                app.UseGiraffeErrorHandler(errorHandler)
                   .UseHttpsRedirection()
                   .UseCors(configureCors)
                   .UseDefaultFiles()
                   .UseStaticFiles()
                   .UseGiraffe(webApp)
                app.UseHangfireDashboard "" |> ignore   
    ()

let configureAppConfiguration  (context: WebHostBuilderContext) (config: IConfigurationBuilder) =
    config        
        .AddJsonFile(sprintf "appsettings.%s.json" context.HostingEnvironment.EnvironmentName ,true)
        .AddEnvironmentVariables() |> ignore
        
        

let configureServices (services : IServiceCollection) =    
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore
    let sp = services.BuildServiceProvider()
    let conf = sp.GetService<IConfiguration>()  
    

    // PascalCase serialization
    let customSettings = JsonSerializerSettings()
    services.AddSingleton<Json.ISerializer>(NewtonsoftJson.Serializer(customSettings)) |> ignore    
    
    let env = services.BuildServiceProvider().GetService<IWebHostEnvironment>()
    match env.IsEnvironment("Test") with
    | false ->
        
        let connStringHang =  (conf.GetSection "HangfireConnectionString").Value
        let opt = MySqlStorageOptions()
        opt.TablesPrefix <- "hangfire"
        opt.TransactionIsolationLevel <- IsolationLevel.ReadCommitted
        opt.PrepareSchemaIfNecessary <- true
        opt.QueuePollInterval <- TimeSpan.FromSeconds(15)
        opt.JobExpirationCheckInterval <- TimeSpan.FromHours(1)
        opt.CountersAggregateInterval <- TimeSpan.FromHours(5)
        let storage = new MySqlStorage(connStringHang, opt)
        GlobalConfiguration.Configuration.UseStorage(storage) |> ignore
        services.AddHangfire(
            fun config ->
                config.UseStorage(storage) |> ignore
            ) |> ignore
        services.AddHangfireServer() |> ignore
    | _ -> () // do not use hangfire in integration testing

    services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(fun options ->
            options.TokenValidationParameters <- TokenValidationParameters(
                ValidateActor = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = "jwtwebapp.net",
                ValidAudience = "jwtwebapp.net",
                IssuerSigningKey = SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)))
            ) |> ignore
        
        
let configureLogging (builder : ILoggingBuilder) =
    builder.AddConsole()
           .AddDebug() |> ignore

[<EntryPoint>]
let main args =
    let webRoot = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") // learn what it is
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder                     
                    //.UseContentRoot(contentRoot) also this ?                    
                    .UseUrls("http://0.0.0.0:5000")
                    .UseUrls("https://0.0.0.0:5001")                                        
                    .UseWebRoot(webRoot)
                    .ConfigureAppConfiguration(configureAppConfiguration)
                    .Configure(Action<IApplicationBuilder> configureApp)                    
                    .ConfigureServices(configureServices)
                    .ConfigureLogging(configureLogging)
                    |> ignore)
        .Build()
        .Run()
    0