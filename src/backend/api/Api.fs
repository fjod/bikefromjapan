module backend.Api


open System
open System.Diagnostics
open System.IO
open System.Text
open System.Security.Claims
open System.IdentityModel.Tokens.Jwt
open System.Threading.Tasks
open Domain
open Domain.Objects
open FsTypes
open Microsoft.Extensions.Configuration
open Microsoft.IdentityModel.Tokens
open Microsoft.AspNetCore.Authentication.JwtBearer
open System.Text.Json
open Microsoft.FSharp.Core
open TsInterfaces
open backend.BikeDownloader
open Microsoft.AspNetCore.Http
open Giraffe
open FSharp.Control.Tasks
open Server.Security
open backend.Security.Jwt
open InterfaceConverters
open Database
open BikeDownloader
open backend.RecurrentJob

let downloader = HttpDownloader()


    
let bikesHandler (startYear : string , endYear : string , manletter : string)=
    fun (next : HttpFunc) (ctx : HttpContext) ->
        task{
             let createResponse input =
                 let q = TS_BikeModelsResult()
                 q.Response <- input
                 q
             let! ret = (downloader :> IDownloadBikes).Get(startYear, endYear, manletter, FakeLog)
            
             let test = Seq.toList ret |> List.map (fun b -> TS_BikeOutput(ManufacturerHelper.ManufacturerToString b.Manufacturer, b.Key, b.Model, b.Image, b.Year, b.Mileage)) |> ResizeArray<TS_BikeOutput>
             let bikes = createResponse test
             return! json bikes next ctx
        }       

let getConnStringFromSettings (ctx:HttpContext) =
    let settings = ctx.GetService<IConfiguration>()
    let section = settings.GetSection "MySqlConnectionString"
    section.Value
    
let bikesModels =
     fun next ctx ->
        task{             
             let! models = Database.DB_getAllBikeModels(getConnStringFromSettings ctx)
             let fsharpModels = Seq.toList models.Payload 
             let manufacturers = fsharpModels |> List.map (fun bike -> ManufacturerHelper.ManufacturerToString bike.Manufacturer) |> List.distinct
             let minYear = (fsharpModels |> List.minBy (fun bike -> bike.StartYear)).StartYear
             let maxYear = (fsharpModels |> List.maxBy (fun bike -> bike.StopYear)).StopYear
             let exportModels = fsharpModels
                                |> List.map ConvertBikeModel
                                |> ResizeArray<TS_BikeModel>
             
             let ret = TS_BikeModels(ResizeArray<string> manufacturers, minYear, maxYear, exportModels)         
             return! json ret next ctx
        }       

let registerHandler  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{
             let connString = getConnStringFromSettings ctx
             let! regData = ctx.BindJsonAsync<TS_RegistrationInput>() |> Async.AwaitTask
             let! userFromDb = Database.DB_getUser (regData.Email, connString) 
             match userFromDb with
               | Ok _ ->
                   let emailTakenError = TS_RegistrationOutput("email is taken", "")
                   return! json emailTakenError next ctx
               | Error _ ->
                    let (email, salt, pass) = createUser(regData.Email, regData.Password)
                    let! result = Database.DB_registerUser (email , salt, pass, connString)                    
                    let tokenResult = generateToken regData.Email
                    let ret = createRegDataOutput result tokenResult.Token
                    return! json ret  next ctx
        }

let handleGetSecured =
    fun (next : HttpFunc) (ctx : HttpContext) ->
        let email = ctx.User.FindFirst ClaimTypes.NameIdentifier
        text ("User " + email.Value + " is authorized to access this resource.") next ctx

let loginHandler  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{
             let! regData = ctx.BindJsonAsync<TS_RegistrationInput>()
             let! userFromDb = Database.DB_getUser (regData.Email, getConnStringFromSettings ctx)
             let ret = userFromDb  |> Result.map (fun u -> verifyPassword regData.Password u.Salt u.Password)
                                   |> Result.bind (fun b -> createToken b regData.Email)
                                   |> createLoginOutput
             return! json ret  next ctx                    
     }

let userAuctDataHandler  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{
             let email = ctx.User.FindFirst ClaimTypes.NameIdentifier
             let! userWithBikes = Database.DB_getUserAuctData (email.Value, getConnStringFromSettings ctx)
             let conv = userWithBikes |> Seq.toList |> Seq.map ConvertBike
             return! json conv  next ctx
     }
        
let auctDataHandler =
     fun (next : HttpFunc) (ctx : HttpContext) ->
        task{
            let connstring = getConnStringFromSettings ctx
            let startYear = ctx.TryGetQueryStringValue "startYear"
            let stopYear = ctx.TryGetQueryStringValue "stopYear"
            let manLetter = ctx.TryGetQueryStringValue "man"
            let page = (ctx.TryGetQueryStringValue "page")
            let page = match page with
                        | Some s -> s |> int
                        | _ -> 0
            let! someValue =
                match (startYear, stopYear, manLetter) with
                | Some start,  Some stop,  Some manLetter -> DB_auctDataFull (int start , int stop, manLetter, page, connstring)
                | Some start, None, Some manLetter -> DB_auctDataFromYear (int start, manLetter, page, connstring)
                | None, Some stop, Some manLetter -> DB_auctDataUntilYear (int stop, manLetter, page, connstring)
                | None, None, Some manLetter -> DB_auctData (manLetter, page, connstring)
                | _ -> Task.FromResult<System.Collections.Generic.List<Bike>>(System.Collections.Generic.List<Bike>(0))
            let ret = someValue |> Seq.map ConvertBike
            return! json ret next ctx
        }

let auctDataForModels  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{
             let! models = ctx.BindJsonAsync<int list>() |> Async.AwaitTask
             let! data = Database.DB_auctDataForModels (models, getConnStringFromSettings ctx)
             let convData = data |> Seq.toList |> Seq.map ConvertBike
             return! json convData next ctx
        }
        
        
let addBikesToUser  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{
             let email = ctx.User.FindFirst ClaimTypes.NameIdentifier
             let! regData = ctx.BindJsonAsync<TS_UserBikeInput>()
             let! userWithBikes = Database.DB_addBikesForUser(email.Value, regData, getConnStringFromSettings ctx)
             return! json userWithBikes  next ctx
     }
        
let getUserBikesModels  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{
             let email = ctx.User.FindFirst ClaimTypes.NameIdentifier             
             let! bikes = Database.DB_getUserModels(email.Value, getConnStringFromSettings ctx)
             let convertedBikes = bikes |> Seq.toList |> Seq.map ConvertBikeModel
             return! json convertedBikes  next ctx
     }
        
let testBikesSaving  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{              
             let openAndDeserialize (f:FileInfo) : System.Collections.Generic.List<Bike> =
                 let content = File.ReadAllText f.FullName
                 let manletter = f.Name[10].ToString()
                 deserialize (content, manletter) |> ResizeArray
                 

             let downloadDt = new DateTime (2022, 7, 28)
             let dayTS = TimeSpan.FromDays 1
             let repo = bikesRepo(getConnStringFromSettings ctx)        
             let dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
             let files = Directory.EnumerateFiles (Path.Combine(dir, "BikeDownloadLogs"))
             let fileInfos = files |> Seq.map FileInfo
                             |> Seq.filter (fun f -> f.CreationTime - downloadDt < dayTS)
                             |> Seq.filter (fun f -> f.Name.Contains "Result" |> not)
                             |> Seq.map openAndDeserialize
             let totalBikeCount = fileInfos |> Seq.map (fun f -> Seq.toList f |> Seq.map (fun b -> (b.Year, b.Key)))
                                            |> Seq.collect id
                                            |> Seq.filter (fun f -> fst f = 0)
                                            |> Seq.iter (fun z -> Debug.WriteLine ($"{fst z}, {snd z}")) |> ignore                                         
             
             // for file in fileInfos do
             //    do! repo.AddBikes file                 
            
             return! json ""  next ctx
     }

let testEmails  =
    fun (next : HttpFunc) (ctx : HttpContext)  ->
        task{
             let ret = TestEmail()
             return! json ret  next ctx
     }