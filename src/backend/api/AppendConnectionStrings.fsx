open System
open System.IO
open System.Text.Json

Console.WriteLine "ConnStrings script start"

type Env =
    | Development
    | Test
    | Production

type Settings = {
    HangfireConnectionString : string
    MySqlConnectionString : string
}

let args = Environment.GetCommandLineArgs()

if (args.Length <= 4) then failwith  "Not enough arguments, must provide three - [env] [hangFireConnString] [mysqlConnString]. [env] can be Production, Test, or anything else for dev env"

if (args.Length > 5) then failwith "Too much arguments, must provide three - [env] [hangFireConnString] [mysqlConnString]. [env] can be Production, Test, or anything else for dev env"

let envContent = args[2]
let hangFireConnString = args[3]
let mysqlConnString = args[4]

Console.WriteLine hangFireConnString
Console.WriteLine envContent

let getFileName(env:Env) =
    match env with
    | Development -> "appsettings.Development.json"
    | Production -> "appsettings.Production.json"
    | Test -> "appsettings.Test.json"
    

let env = match envContent with
                | "Production" -> Production
                | "Test" -> Test
                | _ -> Development

let fileName = getFileName env

let content = JsonSerializer.Serialize { HangfireConnectionString = hangFireConnString; MySqlConnectionString = mysqlConnString }
File.WriteAllText (fileName, content)

Console.WriteLine "ConnStrings script end"