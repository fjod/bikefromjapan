

open Microsoft.FSharp.Core

type Test =
        { EntityId: string }

type Test2 =
        { EntityId: string
          Name: string 
          Length: int }
        
let inline (|HasEntityId|) x =
        fun () -> (^a : (member EntityId: string) x) // as you can see, SRTP syntax is hardly comfortable to use
        
let inline (|HasEntityId2|) x = (^a : (member EntityId: string) x) 

// Then we define function for retrieving this field
let inline entityId (HasEntityId f) = f()

// Another AP, this time for detecting `Name: string`:
let inline (|HasName|) n =
        fun () -> (^a : (member Name: string) n)

// and function for getting the name:
let inline name (HasName f) = f()
    
// Now here's the beauty of it: we can combine them!    
let inline printNameAndId (HasName n & HasEntityId id) = sprintf "name %s id %s" (n()) (id())
    
// Watching it in action:
let test1 = { EntityId = "123" }
let test2 = {Test2.EntityId = "123"; Name = "123"; Length = 2}
    
// Works both with Test and Test2 types since both have `EntityId` field
let a = entityId test1
let b = entityId test2
