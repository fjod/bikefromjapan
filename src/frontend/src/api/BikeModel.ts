import Init from "./init";
import { TsInterfaces } from "../backEndInterfaces";

const fetchBikeModels = async (): Promise<TsInterfaces.ITS_BikeModels> => {
  let rest = Init.initWithoutToken();
  const response = await rest.get(`models`);
  return response.data as TsInterfaces.ITS_BikeModels;
};

const saveUserBikeModels = async (
  models: TsInterfaces.ITS_UserBikeInput
): Promise<void> => {
  let rest = Init.bearerWithRest();
  await rest.post(`addUserModels`, models);
};

export { fetchBikeModels, saveUserBikeModels };
