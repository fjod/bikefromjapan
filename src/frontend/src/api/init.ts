import axios, { AxiosInstance } from "axios";
import TokenHelpers from "./token";

let noToken: AxiosInstance | undefined = undefined;
let withToken: AxiosInstance | undefined = undefined;

const initWithoutToken = (): AxiosInstance => {
  if (noToken === undefined) {
    noToken = axios.create({
      baseURL: `${process.env.REACT_APP_API}`,
      headers: {
        "Content-Type": "text/plain",
      },
    });
  }
  return noToken;
};

const bearerWithRest = (): AxiosInstance => {
  if (withToken === undefined) {
    const token = TokenHelpers.getAccessToken();
    withToken = axios.create({
      baseURL: `${process.env.REACT_APP_API}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
  }
  return withToken;
};

export default { bearerWithRest, initWithoutToken };
