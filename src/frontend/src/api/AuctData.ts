import Init from "./init";
import { TsInterfaces } from "../backEndInterfaces";

interface AuctDataRequest {
  manLetter: string;
  [key: string]: any;
}

const fetchAuctData = async (
  manLetter: string,
  page : number,
  startYear?: number,
  stopYear?: number  
): Promise<TsInterfaces.ITS_BikeOutput[]> => {
  let rest = Init.initWithoutToken();
  const params = new URLSearchParams();
  params.append("man", manLetter.toString());
  params.append("page", page.toString());

  if (startYear !== undefined) params.append("startYear", startYear.toString());
  if (stopYear !== undefined) params.append("stopYear", stopYear.toString());
  const response = await rest.get(`auctData`, { params });
  return response.data as TsInterfaces.ITS_BikeOutput[];
};

const fetchUserAuctData = async (
  email: string
): Promise<TsInterfaces.ITS_BikeOutput[]> => {
  let rest = Init.bearerWithRest();
  const response = await rest.get(`userAuctData`);
  return response.data as TsInterfaces.ITS_BikeOutput[];
};

const fetchUserModels = async (
  email: string
): Promise<TsInterfaces.ITS_BikeModel[]> => {
  let rest = Init.bearerWithRest();
  const response = await rest.get(`userModels`);
  return response.data as TsInterfaces.ITS_BikeModel[];
};

const fetchAuctDataForModels = async (
  models: number[]
): Promise<TsInterfaces.ITS_BikeOutput[]> => {
  let rest = Init.initWithoutToken();
  const response = await rest.post(`auctData`, models);
  return response.data as TsInterfaces.ITS_BikeOutput[];
};

export {
  fetchAuctData,
  fetchUserAuctData,
  fetchUserModels,
  fetchAuctDataForModels,
};
