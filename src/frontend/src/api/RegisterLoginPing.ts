import TokenHelpers from "./token";
import Init from "./init";
import { TsInterfaces } from "../../../frontend/src/backEndInterfaces";

const loginIntoBackend = async (
  input: TsInterfaces.ITS_RegistrationInput
): Promise<TsInterfaces.ITS_RegistrationOutput> => {
  TokenHelpers.removeAccessToken();
  let rest = Init.initWithoutToken();
  const data = JSON.stringify(input);
  const response = await rest.post(`login`, data);
  return response.data as TsInterfaces.ITS_RegistrationOutput;
};

const register = async (
  input: TsInterfaces.ITS_RegistrationInput
): Promise<TsInterfaces.ITS_RegistrationOutput> => {
  TokenHelpers.removeAccessToken();
  let rest = Init.initWithoutToken();
  const data = JSON.stringify(input);
  const response = await rest.post(`register`, data);
  return response.data as TsInterfaces.ITS_RegistrationOutput;
};

const authorizedPing = async (): Promise<string> => {
  let rest = Init.bearerWithRest();
  const response = await rest.get(`authorizedPing`);
  return response.data as string;
};

const ping = async (): Promise<string> => {
  let rest = Init.initWithoutToken();
  const response = await rest.get(`ping`);
  return response.data as string;
};

export { loginIntoBackend, register, authorizedPing, ping };
