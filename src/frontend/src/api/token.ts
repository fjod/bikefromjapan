const getAccessToken = (): string | null => {
  return window.localStorage.getItem("bikesToken");
};

const updateAccessToken = (token: string): void => {
  return localStorage.setItem("bikesToken", token);
};

const removeAccessToken = (): void => {
  return localStorage.removeItem("bikesToken");
};

const TokenHelpers = {
  getAccessToken,
  updateAccessToken,
  removeAccessToken,
};

export default TokenHelpers;
