//     This code was generated by a Reinforced.Typings tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.

export module TsInterfaces {
	export interface ITS_UserBikeInput
	{
		BikeIds: number[];
	}
	export interface ITS_BikeInput
	{
		Name: string;
		StartYear: number;
		EndYear: number;
		Manufacturer: string;
		Id: number;
	}
	export interface ITS_BikeModel
	{
		Id: number;
		Manufacturer: string;
		Name: string;
		StartYear: number;
		StopYear: number;
	}
	export interface ITS_BikeModels
	{
		Manufacturers: string[];
		MinYear: number;
		MaxYear: number;
		BikeModels: TsInterfaces.ITS_BikeModel[];
	}
	export interface ITS_BikeOutput
	{
		Manufacturer: string;
		Key: string;
		Name: string;
		Image: string;
		Year: number;
		Mileage: number;
	}
	export interface ITS_RegistrationInput
	{
		Email: string;
		Password: string;
	}
	export interface ITS_RegistrationOutput
	{
		Message: string;
		Token: string;
	}
	export interface ITS_BikeModelsResult
	{
		Response: TsInterfaces.ITS_BikeOutput[];
	}
	export interface ITS_CommonBackendResult
	{
		Message: string;
	}
}
