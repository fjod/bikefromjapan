import { atom } from "recoil";
import User, {
  LoginRegisterStatus,
  UserStatus,
  UserWorkType,
} from "../Domain/User";
import TokenHelpers from "../api/token";
import jwt_decode from "jwt-decode";

const loginRegisterState = atom<LoginRegisterStatus>({
  key: "loginRegisterStatus", // unique ID (with respect to other atoms/selectors)
  default: LoginRegisterStatus.Empty, // default value (aka initial value)
});

const currentToken = TokenHelpers.getAccessToken();

const tokenState = atom<string | null>({
  key: "tokenState", // unique ID (with respect to other atoms/selectors)
  default: currentToken, // default value (aka initial value)
});

const getEmailFromToken = () => {
  if (tokenState === null) {
    return null;
  }

  const value = TokenHelpers.getAccessToken();
  if (value) {
    const decoded = jwt_decode(value!);

    console.log(decoded);
  }

  return "email";
};

export const emptyUser: User = { email: "", status: UserStatus.NotLoggedIn };
export const loggedUser: User = {
  email: `${getEmailFromToken()}`,
  status: UserStatus.LoggedIn,
};

const userState = atom<User>({
  key: "userState", // unique ID (with respect to other atoms/selectors)
  default: currentToken === null ? emptyUser : loggedUser, // default value (aka initial value)
});

const userWorkState = atom<UserWorkType>({
  key: "userWorkType", // unique ID (with respect to other atoms/selectors)
  default:
    currentToken === null
      ? UserWorkType.LookAtOtherModels
      : UserWorkType.BrowseOwnModels, // default value (aka initial value)
});

export { loginRegisterState, userState, userWorkState };
