import { atom } from "recoil";
import { TsInterfaces } from "../backEndInterfaces";

const fullAuctData = atom<TsInterfaces.ITS_BikeOutput[] | undefined>({
  key: "fullAuctData", // unique ID (with respect to other atoms/selectors)
  default: undefined, // default value (aka initial value)
});

const selectedAuctCard = atom<TsInterfaces.ITS_BikeOutput | undefined>({
  key: "selectedAuctCard", // unique ID (with respect to other atoms/selectors)
  default: undefined, // default value (aka initial value)
});

export { fullAuctData, selectedAuctCard };
