import { atom, selector } from "recoil";
import { TsInterfaces } from "../backEndInterfaces";
import { userState } from "./UserState";
import { UserStatus } from "../Domain/User";

// все модели (вообще все какие есть)
const allBikeModels = atom<TsInterfaces.ITS_BikeModels | undefined>({
  key: "allBikeModels", // unique ID (with respect to other atoms/selectors)
  default: undefined, // default value (aka initial value)
});

// Выбранное в списке (но не добавленное на сервер)
const serverUserModels = atom<TsInterfaces.ITS_BikeModel[] | undefined>({
  key: "userModels", // unique ID (with respect to other atoms/selectors)
  default: undefined, // default value (aka initial value)
});

const bikeModelStartYear = atom<number | undefined>({
  key: "selectedStartYear", // unique ID (with respect to other atoms/selectors)
  default: undefined, // default value (aka initial value)
});

const bikeModelEndYear = atom<number | undefined>({
  key: "selectedEndYear", // unique ID (with respect to other atoms/selectors)
  default: undefined, // default value (aka initial value)
});

const bikeModelManufacturer = atom<string | undefined>({
  key: "selectedManufacturer", // unique ID (with respect to other atoms/selectors)
  default: undefined, // default value (aka initial value)
});

const bikeYearRangeError = selector({
  key: "bikeYearRangeError",
  get: ({ get }) => {
    const start = get(bikeModelStartYear);
    const end = get(bikeModelEndYear);

    if (start !== undefined && end !== undefined) {
      return start >= end;
    }

    return false;
  },
});

function squash(arr: any) {
  var tmp = [];
  for (var i = 0; i < arr.length; i++) {
    if (tmp.indexOf(arr[i]) == -1) {
      tmp.push(arr[i]);
    }
  }
  return tmp;
}

// имена моделей для списка чтобы выбрать с учетом уже выбранных
const bikeModelNames = selector<string[] | undefined>({
  key: "bikeModelNames",
  get: ({ get }) => {
    const models = get(allBikeModels);
    const start = get(bikeModelStartYear);
    const end = get(bikeModelEndYear);
    const man = get(bikeModelManufacturer);
    const selectedModels = get(serverUserModels);
    const userStatus = get(userState);    

    if (userStatus.status === UserStatus.NotLoggedIn) {
      if (models === undefined) return [];
      if (man === undefined) return [];
      if (start === undefined && end === undefined) return [];

      if (start !== undefined && end === undefined) {
        return models.BikeModels.filter(
          (m) => m.Manufacturer === man && m.StartYear >= start
        ).map((m) => m.Name);
      }

      if (end !== undefined && start === undefined) {
        return models.BikeModels.filter(
          (m) => m.Manufacturer === man && m.StopYear <= end
        ).map((m) => m.Name);
      }

      return models.BikeModels.filter(
        (m) =>
          m.Manufacturer === man && m.StopYear >= end! && m.StartYear >= start!
      ).map((m) => m.Name);
    }

    const selectedModelNames = selectedModels?.map((m) => m.Name);
    // сохраненные модели с сервера, которых еще нет в выбранных     

    if (models === undefined) return selectedModelNames;
    if (man === undefined) return selectedModelNames;
    if (start === undefined && end === undefined) return selectedModelNames;

    if (start !== undefined && end === undefined) {
      const m = models.BikeModels.filter(
        (m) => m.Manufacturer === man && m.StartYear >= start
      ).map((m) => m.Name);
      return selectedModelNames === undefined ? m : squash(m.concat(selectedModelNames));
    }

    if (end !== undefined && start === undefined) {
      const m = models.BikeModels.filter(
        (m) => m.Manufacturer === man && m.StopYear <= end
      ).map((m) => m.Name);
      return selectedModelNames === undefined ? m : squash(m.concat(selectedModelNames));
    }

    const m = models.BikeModels.filter(
      (m) =>
        m.Manufacturer === man && m.StopYear >= end! && m.StartYear >= start!
    ).map((m) => m.Name);
    return selectedModelNames === undefined ? m : squash(m.concat(selectedModelNames));
  },
});

export {
  allBikeModels,
  bikeModelStartYear,
  bikeModelEndYear,
  bikeModelManufacturer,
  bikeYearRangeError,
  bikeModelNames,
  serverUserModels
};
