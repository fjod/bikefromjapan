open System
open System.IO

Console.WriteLine "Fix backEndInterfaces script starts"

let file = File.ReadAllText "backEndInterfaces.ts"
let tryFindCorrectLine = file.Contains "export module TsInterfaces"

if tryFindCorrectLine then
    Console.WriteLine "backEndInterfaces.ts is already correct, exiting.."
    else
        let correct = file.Replace ("module TsInterfaces", "export module TsInterfaces")
        File.WriteAllText ("backEndInterfaces.ts", correct)
        Console.WriteLine "Fixed backEndInterfaces.ts, script ends"
