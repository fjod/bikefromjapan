﻿import React, {useState} from "react";
import {useRecoilState} from "recoil";
import {UserStatus, UserWorkType} from "../../Domain/User";
import {userState, userWorkState} from "../../State/UserState";
import type { RadioChangeEvent } from 'antd';
import { Radio } from 'antd';
import "./UserModeCheckboxes.css";

const UserModeCheckboxes: React.FC = () => {    
    const [currentWorkState, setUserWorkState] = useRecoilState<UserWorkType>(userWorkState);
    const [value, setValue] = useState(2);

    const onChange = (e: RadioChangeEvent) => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
        if (e.target.value === 1){
            setUserWorkState(UserWorkType.BrowseOwnModels);
        }
        if (e.target.value === 2){
            setUserWorkState(UserWorkType.LookAtOtherModels)
        }    
    };     
    
    return  <div className=".columnWithRows">
        <Radio.Group onChange={onChange} value={value}>
            <Radio value={2}>All models</Radio>
            <Radio value={1} > Selected models</Radio>     
        </Radio.Group>          
        </div>
};

export default UserModeCheckboxes;