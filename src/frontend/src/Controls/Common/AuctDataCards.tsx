import { TsInterfaces } from "../../backEndInterfaces";
import React, {useState} from "react";
import { Card } from "antd";
import AuctCard from "./AuctCard";
import { useRecoilState } from "recoil";
import { fullAuctData } from "../../State/AuctDataState";
import InfiniteScroll from 'react-infinite-scroll-component';
import {bikeModelEndYear, bikeModelManufacturer, bikeModelStartYear, serverUserModels} from "../../State/BikeModelState";
import {UserWorkType} from "../../Domain/User";
import {userWorkState} from "../../State/UserState";
import {fetchAuctData, fetchAuctDataForModels} from "../../api/AuctData";
import {toast} from "react-toastify";

const AuctDataCards: React.FC = () => {  

    const [startYear] = useRecoilState(bikeModelStartYear);
    const [stopYear] = useRecoilState(bikeModelEndYear);
    const [selectedManufacturer] = useRecoilState<string | undefined>(bikeModelManufacturer);
    const [page, setPage] = useState(0);
    const [currentUserWorkState] =  useRecoilState<UserWorkType>(userWorkState);
    const [userBikeModels] = useRecoilState(serverUserModels);
    const [auctData, setAuctData] = useRecoilState<TsInterfaces.ITS_BikeOutput[] | undefined>(fullAuctData);

    const updateAuctData = async () => {
        if (currentUserWorkState === UserWorkType.BrowseOwnModels) {
            if (userBikeModels !== undefined) {
                const ids = userBikeModels.map((b) => b.Id);
                const models = await fetchAuctDataForModels(ids);
                setAuctData(models);
            } else {
                toast("Please select some models");
            }
        }
        if (currentUserWorkState === UserWorkType.LookAtOtherModels) {
            if (selectedManufacturer != undefined) {
               
                let incrPage = page + 1;
                setPage(incrPage);
                const models = await fetchAuctData(selectedManufacturer.at(0)!, incrPage, startYear, stopYear);
                if (auctData === undefined){
                    setAuctData(models);
                }
                else
                {   
                    let unique = models.filter(m => auctData.find(z => z.Key == m.Key) === undefined);
                    setAuctData(auctData?.concat(unique));
                }
            } else { 
                toast("Please select manufacturer + years");
            }
        }
    };
    
    const getLength = () => {
        if (auctData === undefined) return 0;
        return auctData?.length;
    }
    
    return (
      <InfiniteScroll
          dataLength={getLength()} //This is important field to render the next data
          next={updateAuctData}
          hasMore={true}
          loader={<h4>Loading...</h4>}
          endMessage={
              <p style={{ textAlign: 'center' }}>
                  <b>Yay! You have seen it all</b>
              </p>
          }
      >
          <Card
              title="Auction data"
              style={{ fontSize: "xxx-large" }}
              key={"penisBenis"}
          >
              {auctData?.map((m) => {
                  return <AuctCard model={m} key={m.Key} />;
              })}
          </Card>
      </InfiniteScroll>
   
  );
};

export default AuctDataCards;
