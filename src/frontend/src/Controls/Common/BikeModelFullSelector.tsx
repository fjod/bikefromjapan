import React, { useEffect } from "react";
import { useRecoilState } from "recoil";
import { TsInterfaces } from "../../backEndInterfaces";
import {
    allBikeModels,
    bikeModelEndYear,
    bikeModelManufacturer,
    bikeModelNames,
    bikeModelStartYear,
    serverUserModels
} from "../../State/BikeModelState";
import { fetchUserModels, fetchUserAuctData } from "../../api/AuctData";
import { isMobile } from "react-device-detect";
import { userState } from "../../State/UserState";

import { fullAuctData } from "../../State/AuctDataState";
import "./BikeModelFullSelector.css";

import ManufacturerAndYearSelector from "./ManufacturerAndYearSelector";
import BikeModelSelector from "./BikeModelSelector";
import User, { UserStatus } from "../../Domain/User";
import { fetchBikeModels } from "../../api/BikeModel";

//Загружает все модели, показывает пользователю выбор производителя, года выпуска
// Модели фильтруются из списка всех по выбранному году и производителю
const BikeModelFullSelector: React.FC = () => {
  const [, setBikeModels] = useRecoilState<TsInterfaces.ITS_BikeModels | undefined>(allBikeModels);

  const [, setUserBikeModels] = useRecoilState< TsInterfaces.ITS_BikeModel[] | undefined>(serverUserModels);

  const [userStateValue] = useRecoilState<User>(userState);

  const [, setAuctData] = useRecoilState<
    TsInterfaces.ITS_BikeOutput[] | undefined
  >(fullAuctData);

  useEffect(() => {
    const fetchAllModels = async () => {
      const data = await fetchBikeModels();
      setBikeModels(data);
    };

    const getUserModels = async () => {
      const data =
        userStateValue.status === UserStatus.LoggedIn
          ? await fetchUserModels(userStateValue.email)
          : [];
        setUserBikeModels(data);
    };

    const getUserAuctData = async () => {
      const data = await fetchUserAuctData(userStateValue.email);
      setAuctData(data);
    };

    if (userStateValue.status === UserStatus.LoggedIn) {
      getUserModels().catch(console.error);
      fetchAllModels().catch(console.error);
    } else {
      console.log("fetching all models");
      fetchAllModels().catch(console.error);
    }
  }, []);

  return (
    //<div className={isMobile ? "selectorMobile" : "selectorPc"}>
      <div >
      <ManufacturerAndYearSelector />
      <BikeModelSelector />
    </div>
  );
};

export default BikeModelFullSelector;
