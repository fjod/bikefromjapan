import React, { useState } from "react";
import { LoginRegisterStatus } from "../../Domain/User";
import { Button, Input } from "antd";

type loginRegisterProps = {
  loginRegister: LoginRegisterStatus;
  enterLoginRegisterClick: (l: string, p: string) => void;
  authProcessStarted: boolean;
};

const LoginRegisterInputs: React.FC<loginRegisterProps> = ({
  loginRegister,
  enterLoginRegisterClick,
  authProcessStarted,
}) => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  return (
    <div
      className="loginRegister"
      hidden={loginRegister === LoginRegisterStatus.Empty}
    >
      <Button
        onClick={() => enterLoginRegisterClick(login, password)}
        loading={authProcessStarted}
      >
        {loginRegister === LoginRegisterStatus.Logging
          ? "Log In"
          : loginRegister === LoginRegisterStatus.Registering
          ? "Register"
          : ""}
      </Button>
      <Input
        placeholder="Login"
        id="loginInput"
        onChange={(e) => setLogin(e.target.value)}
      />
      <Input
        placeholder="Password"
        id="passInput"
        onChange={(e) => setPassword(e.target.value)}
      />
    </div>
  );
};

export default LoginRegisterInputs;
