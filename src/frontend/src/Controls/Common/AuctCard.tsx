import React from "react";
import { PhoneOutlined } from "@ant-design/icons";
import { Card, Button } from "antd";
import { TsInterfaces } from "../../backEndInterfaces";
const { Meta } = Card;

type IProps = {
  model: TsInterfaces.ITS_BikeOutput;
};

function openInNewTab(href: string) {
  console.log(href);
  Object.assign(document.createElement("a"), {
    target: "_blank",
    rel: "noopener noreferrer",
    href: href,
  }).click();
}

const AuctCard: React.FC<IProps> = ({ model }) => {
  return (
    <Card.Grid style={{ width: 350 }}>
      <Card
        style={{ width: 300 }}
        cover={<img src={model.Image} />}
        actions={[
          <Button
            onClick={(event) => {
              openInNewTab("https://projapan.ru/bike/" + model.Key);
            }}
          >
            <PhoneOutlined key="setting" />
            See full info
          </Button>,
        ]}
      >
        <Meta
          title={model.Manufacturer + " " + model.Name + " " + model.Year}
          description={model.Mileage + " km."}
        />
      </Card>
    </Card.Grid>
  );
};

export default AuctCard;
