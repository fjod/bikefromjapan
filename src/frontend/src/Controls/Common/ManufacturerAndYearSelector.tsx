import React from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { isMobile } from "react-device-detect";
import {
  bikeModelEndYear,
  bikeModelManufacturer,
  bikeModelStartYear,
  allBikeModels,
  bikeYearRangeError,
} from "../../State/BikeModelState";
import { TsInterfaces } from "../../backEndInterfaces";
import "./ManufacturerAndYearSelector.css";
import { Dropdown, InputNumber, Menu, Space } from "antd";
import { userWorkState } from "../../State/UserState";
import { UserWorkType } from "../../Domain/User";

const ManufacturerAndYearSelector: React.FC = () => {
  const [bikeModels] = useRecoilState<TsInterfaces.ITS_BikeModels | undefined>(allBikeModels);

  const yearError = useRecoilValue(bikeYearRangeError);

  const [selectedManufacturer, setBikeManufacturer] = useRecoilState<
    string | undefined
  >(bikeModelManufacturer);

  const [currentUserWorkState, setUserWorkState] =
    useRecoilState<UserWorkType>(userWorkState);

  let i = 0;
  const manufacturerToDropdownItem = (m: string) => {
    return {
      key: (i++).toString(),
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => {
            setBikeManufacturer(m);          
          }}
        >
          {m}
        </a>
      ),
    };
  };

  const manufacturersMenu = (
    <Menu
      items={bikeModels?.Manufacturers?.map((m) =>
        manufacturerToDropdownItem(m)
      )}
    />
  );

  const [, setBikeStartYear] = useRecoilState<number | undefined>(
    bikeModelStartYear
  );

  const [, setBikeEndYear] = useRecoilState<number | undefined>(
    bikeModelEndYear
  );

  const ManufacturerDropDownView = () => {
    return selectedManufacturer === undefined
      ? "Select Manufacturer"
      : selectedManufacturer;
  };

  const getCss = () => {
    if (isMobile) {
      if (currentUserWorkState === UserWorkType.LookAtOtherModels) {
        return "container mobileBikeContainer blackBorder";
      } else {
        return "container mobileBikeContainer";
      }
    } else {
      if (currentUserWorkState === UserWorkType.LookAtOtherModels) {
        return "container blackBorder";
      } else {
        return "container";
      }
    }
  };

  return (
    <div
      className={getCss()}
      onClick={(event) => setUserWorkState(UserWorkType.LookAtOtherModels)}
    >
      <Dropdown overlay={manufacturersMenu} arrow={true}>
        <a onClick={(e) => e.preventDefault()}>
          <Space className="dropDownFont"> {ManufacturerDropDownView()} </Space>
        </a>
      </Dropdown>
      <div className="mobileInnerYears">
        <h3
          className={isMobile ? "yearSection mobileYearSection" : "yearSection"}
        >
          {" "}
          Start Year:{" "}
        </h3>
        <div>
          <InputNumber
            status={yearError ? "error" : undefined}
            className={
              isMobile
                ? "yearFont yearSection mobileYearInput"
                : "yearFont yearSection"
            }
            max={bikeModels?.MaxYear}
            min={bikeModels?.MinYear}
            onChange={setBikeStartYear}
          />
        </div>

        <h3
          className={isMobile ? "yearSection mobileYearSection" : "yearSection"}
        >
          {" "}
          End Year:{" "}
        </h3>
        <div>
          <InputNumber
            status={yearError ? "error" : undefined}
            className={
              isMobile
                ? "yearFont yearSection mobileYearInput"
                : "yearFont yearSection"
            }
            max={bikeModels?.MaxYear}
            min={bikeModels?.MinYear}
            onChange={setBikeEndYear}
          />
        </div>
      </div>
    </div>
  );
};

export default ManufacturerAndYearSelector;
