import React, { useState } from "react";
import { useRecoilState } from "recoil";
import {
  emptyUser,
  loggedUser,
  loginRegisterState,
  userState,
} from "../../State/UserState";
import { BrowserView, MobileView } from "react-device-detect";
import BikeHeader from "../PC/BikeHeader";
import MobileBikeHeader from "../Mobile/MobileBikeHeader";
import { Layout } from "antd";
import { LoginRegisterStatus, UserStatus } from "../../Domain/User";
import {
  authorizedPing,
  loginIntoBackend,
  ping,
  register,
} from "../../api/RegisterLoginPing";
import { toast } from "react-toastify";
import TokenHelpers from "../../api/token";

const HeaderFC: React.FC = () => {
  const [collapsed, setCollapsed] = React.useState(false);

  const [loginRegister, setLoginRegister] = useRecoilState(loginRegisterState);

  const [authProcessStarted, setAuthProcessStarted] = useState(false);

  const [user, setUser] = useRecoilState(userState);

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  const loginMenuClick = () => {
    if (loginRegister === LoginRegisterStatus.Logging) {
      setLoginRegister(LoginRegisterStatus.Empty);
      setUser(emptyUser);
    } else if (user.status === UserStatus.NotLoggedIn) {
      setLoginRegister(LoginRegisterStatus.Logging);
    } else if (user.status === UserStatus.LoggedIn) {
      setLoginRegister(LoginRegisterStatus.Empty);
      setUser(emptyUser);
      TokenHelpers.removeAccessToken();
      toast("logged you out");
    }
  };

  const registerMenuClick = () => {
    if (loginRegister === LoginRegisterStatus.Registering) {
      setLoginRegister(LoginRegisterStatus.Empty);
      setUser(emptyUser);
    } else if (user.status === UserStatus.NotLoggedIn) {
      setLoginRegister(LoginRegisterStatus.Registering);
    }
  };

  const checkConnection = async () => {
    const ret = await authorizedPing();
    toast(ret);
  };

  const checkConnectionPing = async () => {
    const ret = await ping();
    console.log(ret);
    toast(ret);
  };

  const enterLoginRegisterClick = async (login: string, password: string) => {
    setAuthProcessStarted(true);

    if (loginRegister === LoginRegisterStatus.Logging) {
      const loginResult = await loginIntoBackend({
        Email: login!,
        Password: password!,
      });
      toast(loginResult.Message);
      if (loginResult.Token !== "") {
        TokenHelpers.updateAccessToken(loginResult.Token);
        setUser(loggedUser);
      }
    }
    if (loginRegister === LoginRegisterStatus.Registering) {
      const loginResult = await register({
        Email: login!,
        Password: password!,
      });
      if (loginResult.Token !== "") {
        TokenHelpers.updateAccessToken(loginResult.Token);
        setUser(loggedUser);
      }
      toast(loginResult.Message);
    }

    const token = TokenHelpers.getAccessToken();
    if (token === null) {
      setUser(emptyUser);
    } else {
      setUser(loggedUser);
    }

    setLoginRegister(LoginRegisterStatus.Empty);
    setAuthProcessStarted(false);
  };

  return (
    <Layout>
      <BrowserView>
        <BikeHeader
          user={user}
          loginMenuClick={loginMenuClick}
          registerMenuClick={registerMenuClick}
          checkConnection={checkConnection}
          checkConnectionPing={checkConnectionPing}
          loginRegister={loginRegister}
          enterLoginRegisterClick={enterLoginRegisterClick}
          authProcessStarted={authProcessStarted}
        />
      </BrowserView>
      <MobileView>
        <MobileBikeHeader
          toggleCollapsed={toggleCollapsed}
          collapsed={collapsed}
          user={user}
          loginMenuClick={loginMenuClick}
          registerMenuClick={registerMenuClick}
          checkConnection={checkConnection}
          checkConnectionPing={checkConnectionPing}
          loginRegister={loginRegister}
          enterLoginRegisterClick={enterLoginRegisterClick}
          authProcessStarted={authProcessStarted}
        />
      </MobileView>
    </Layout>
  );
};

export default HeaderFC;
