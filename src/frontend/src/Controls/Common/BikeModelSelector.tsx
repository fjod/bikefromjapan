import {Button} from "antd";
import {MultiSelect} from "react-multi-select-component";
import React, {useEffect, useState} from "react";
import {useRecoilState, useRecoilValue} from "recoil";
import {
    allBikeModels,
    bikeModelEndYear,
    bikeModelManufacturer,
    bikeModelNames,
    bikeModelStartYear,
    serverUserModels
} from "../../State/BikeModelState";
import "./BikeModelSelector.css";
import {isMobile} from "react-device-detect";
import {UserStatus, UserWorkType} from "../../Domain/User";
import {userState, userWorkState} from "../../State/UserState";
import {saveUserBikeModels} from "../../api/BikeModel";
import {toast} from "react-toastify";
import {TsInterfaces} from "../../backEndInterfaces";
import {fullAuctData} from "../../State/AuctDataState";
import {fetchAuctData, fetchAuctDataForModels} from "../../api/AuctData";
import UserModeCheckboxes from "./UserModeCheckboxes";

const BikeModelSelector: React.FC = () => {
    const modelNames = useRecoilValue(bikeModelNames);

    const [userModels, _] = useRecoilState(serverUserModels);

    const [userIsLoggedState] = useRecoilState(userState);
    const [currentUserWorkState,] = useRecoilState<UserWorkType>(userWorkState);

    // все модели для выбора (с сервера и здесь)
    const [bikeModels] = useRecoilState(allBikeModels);

    //имена моделей для выпадающего списка
    const [bikeNames, setBikeNames] = useState<({ label: string, value: string, disabled?: undefined } | { label: string, value: string, disabled: boolean })[]>([]);
   
    // выбранные модели в выпадающем списке
    const [selected,setSelected] = useState<({ label: string, value: string, disabled?: undefined } | { label: string, value: string, disabled: boolean })[]>([]);
    
    // для запроса объявлений 
    const [startYear] = useRecoilState(bikeModelStartYear);
    const [stopYear] = useRecoilState(bikeModelEndYear);
    const [selectedManufacturer] = useRecoilState<string | undefined>(bikeModelManufacturer);
    const [page, setPage] = useState(0);
    const [auctData, setAuctData] = useRecoilState<TsInterfaces.ITS_BikeOutput[] | undefined>(fullAuctData);
    const updateAuctData = async () => {
        if (auctData !== undefined) setAuctData([]);
        if (currentUserWorkState === UserWorkType.BrowseOwnModels) {
            const selectedNames = selected.map(s => s.label).filter(f => f !== "<- your bikes from server");
            const userSelectedModels = bikeModels?.BikeModels.filter((m) => selectedNames.includes(m.Name));
            if (userSelectedModels !== undefined) {
                const ids = userSelectedModels.map((b) => b.Id);
                console.log(ids);
                const models = await fetchAuctDataForModels(ids);
                setAuctData(models);
            } else {
                toast("Please select some models");
            }
        }
        if (currentUserWorkState === UserWorkType.LookAtOtherModels) {
            if (selectedManufacturer != undefined) {
                const models = await fetchAuctData(selectedManufacturer.at(0)!, page, startYear, stopYear);
                setPage(page + 1);
                setAuctData(models);
            } else {
                toast("Please select manufacturer + years");
            }
        }
    };


    // при показе формы загружаем в список выбора все модели
    useEffect(() => {
        const bikeModels: ({ label: string, value: string, disabled?: undefined } | { label: string, value: string, disabled: boolean })[] = [];
        modelNames?.forEach((m) =>
            bikeModels.push({label: m, value: m, disabled: false})
        );
        setBikeNames(bikeModels);

        const tempUserModels: ({ label: string, value: string, disabled?: undefined } | { label: string, value: string, disabled: boolean })[] = [];
        userModels?.forEach((m) =>
            tempUserModels.push({label: m.Name, value: m.Name, disabled: false}));
        if (tempUserModels.length > 0){
            tempUserModels.push({label: "<- your bikes from server", value: "<- your bikes from server", disabled: true});
        }
        setSelected(tempUserModels);
        
    }, [modelNames]);
    
    const saveUserBikesToServer = () => {
        const selectedNames = selected.map(s => s.label).filter(f => f !== "<- your bikes from server");
        const userSelectedModels = bikeModels?.BikeModels.filter((m) => selectedNames.includes(m.Name));
        const ids = userSelectedModels?.map((b) => b.Id);
        if (ids !== undefined) {
            saveUserBikeModels({BikeIds: ids}).then((r) => console.log(r));
            toast("Saved your models!");
        }
    };


    return (
        <div>           
            <div>
                <MultiSelect
                    className={"selectorMobile"}
                    options={bikeNames}
                    value={selected}                   
                    onChange={setSelected}
                    hasSelectAll = {false}
                    labelledBy={"112233"}
                />
            </div>

            <UserModeCheckboxes/>

            <div className={isMobile ? "buttonsMobile" : ""}>
                <Button
                    type={"primary"}
                    size={"large"}
                    className={userIsLoggedState.status === UserStatus.LoggedIn ? "" : "buttonMobile"}
                    onClick={updateAuctData}
                >
                    Load auct data
                </Button>
                {userIsLoggedState.status === UserStatus.LoggedIn ? (
                    <Button
                        type={"primary"}
                        size={"large"}
                        className={"button"}
                        onClick={saveUserBikesToServer}
                    >
                        Save to your models
                    </Button>
                ) : (
                    <div/>
                )}
            </div> 
        </div>
    );
};

export default BikeModelSelector;
