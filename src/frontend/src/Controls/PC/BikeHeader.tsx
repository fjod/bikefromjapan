import React from "react";
import { Layout, Menu } from "antd";
import User, { LoginRegisterStatus, UserStatus } from "../../Domain/User";
import "antd/dist/antd.css";
import "../../index.css";
import "./BikeHeader.css";
import LoginRegisterInputs from "../Common/LoginRegisterInputs";
import { ToastContainer } from "react-toastify";

const { Header } = Layout;

type PcProps = {
  user: User;
  loginMenuClick: () => void;
  registerMenuClick: () => void;
  checkConnection: () => void;
  checkConnectionPing: () => void;
  loginRegister: LoginRegisterStatus;
  enterLoginRegisterClick: (l: string, p: string) => Promise<void>;
  authProcessStarted: boolean;
};

const BikeHeader: React.FC<PcProps> = ({
  user,
  loginMenuClick,
  registerMenuClick,
  checkConnection,
  checkConnectionPing,
  loginRegister,
  enterLoginRegisterClick,
  authProcessStarted,
}) => {
  return (
    <Header>
      <div />
      <Menu mode="horizontal" selectable={false} className="menuStyle">
        <Menu.Item disabled={true} key="1">
          {user.status === UserStatus.NotLoggedIn ? (
            <div>
              <h1 className="userStatusLabelNotLogged">NotLogged</h1>
            </div>
          ) : (
            <h1 className="userStatusLabelLogged">{user.email}</h1>
          )}
        </Menu.Item>
        <Menu.Item key="2" onClick={loginMenuClick}>
          {user.status === UserStatus.NotLoggedIn ? (
            <h1>LogIn</h1>
          ) : (
            <h1>LogOut</h1>
          )}
        </Menu.Item>
        <Menu.Item key="3" onClick={registerMenuClick}>
          <h1>Register</h1>
        </Menu.Item>

        <LoginRegisterInputs
          loginRegister={loginRegister}
          enterLoginRegisterClick={enterLoginRegisterClick}
          authProcessStarted={authProcessStarted}
        />

        <Menu.Item key="4" onClick={checkConnection}>
          <h1>AuthPing</h1>
        </Menu.Item>
        <Menu.Item key="5" onClick={checkConnectionPing}>
          <h1>Ping</h1>
        </Menu.Item>
      </Menu>
      <ToastContainer />
    </Header>
  );
};

export default BikeHeader;
