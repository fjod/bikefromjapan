import { Menu, Button, Input } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import React from "react";
import User, { LoginRegisterStatus, UserStatus } from "../../Domain/User";
import "./MobileBikeHeader.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import LoginRegisterInputs from "../Common/LoginRegisterInputs";

type MobileProps = {
  toggleCollapsed: () => void;
  collapsed: boolean;
  user: User;
  loginMenuClick: () => void;
  registerMenuClick: () => void;
  checkConnection: () => void;
  checkConnectionPing: () => void;
  loginRegister: LoginRegisterStatus;
  enterLoginRegisterClick: (l: string, p: string) => Promise<void>;
  authProcessStarted: boolean;
};

const MobileBikeHeader: React.FC<MobileProps> = ({
  toggleCollapsed,
  collapsed,
  user,
  loginMenuClick,
  registerMenuClick,
  checkConnection,
  checkConnectionPing,
  loginRegister,
  enterLoginRegisterClick,
  authProcessStarted,
}) => {
  return (
    <div>
      <Button
        type="primary"
        onClick={toggleCollapsed}
        style={{ marginBottom: 16 }}
      >
        {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </Button>
      {collapsed ? (
        ""
      ) : (
        <Menu mode="inline" inlineCollapsed={collapsed}>
          <Menu.Item disabled={true} key="1">
            {user.status === UserStatus.NotLoggedIn ? (
              <div>
                <h1 className="userStatusLabelNotLogged">NotLogged</h1>
              </div>
            ) : (
              <h1 className="userStatusLabelLogged">{user.email}</h1>
            )}
          </Menu.Item>

          <Menu.Item key="2" onClick={loginMenuClick}>
            {user.status === UserStatus.NotLoggedIn ? (
              <h1>LogIn</h1>
            ) : (
              <h1>LogOut</h1>
            )}
          </Menu.Item>
          <Menu.Item key="3" onClick={registerMenuClick}>
            <h1>Register</h1>
          </Menu.Item>

          <LoginRegisterInputs
            loginRegister={loginRegister}
            enterLoginRegisterClick={enterLoginRegisterClick}
            authProcessStarted={authProcessStarted}
          />

          <Menu.Item key="4" onClick={checkConnection}>
            <h1>AuthPing</h1>
          </Menu.Item>
          <Menu.Item key="5" onClick={checkConnectionPing}>
            <h1>Ping</h1>
          </Menu.Item>
        </Menu>
      )}
      <ToastContainer />
    </div>
  );
};

export default MobileBikeHeader;
