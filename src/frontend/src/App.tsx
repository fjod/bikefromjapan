import React from "react";
import { Layout } from "antd";
import "./App.css";
import "antd/dist/antd.css";
import "./index.css";
import HeaderFC from "./Controls/Common/HeaderFC";
import BikeModelFullSelector from "./Controls/Common/BikeModelFullSelector";
import AuctDataCards from "./Controls/Common/AuctDataCards";

function App() {
  return (
    <Layout>
      <HeaderFC />
      <BikeModelFullSelector />
      <AuctDataCards />
    </Layout>
  );
}

export default App;
