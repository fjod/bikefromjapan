export enum UserStatus {
  LoggedIn,
  NotLoggedIn,
}

interface User {
  email: string;
  status: UserStatus;
}

export enum LoginRegisterStatus {
  Registering = 0,
  Logging,
  Empty,
}

export enum UserWorkType {
  BrowseOwnModels,
  LookAtOtherModels,
}

export default User;
